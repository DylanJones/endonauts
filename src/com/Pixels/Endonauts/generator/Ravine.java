package com.Pixels.Endonauts.generator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.Random;

public class Ravine implements Generator {
    @Override
    public void generate(Random r, World w) {
        for (int count = r.nextInt(4) + 1; count > 0; count--) {
            Vector delta = new Vector(r.nextGaussian() * 2 - 1, 0, r.nextGaussian() * 2 - 1);
            Vector vel = new Vector(r.nextInt(3) - 1, 0, r.nextInt(3) - 1);
            Location l = Structure.createGroundLoc(r, w);
            float depth = r.nextInt(24) + 4;
            float width = r.nextInt(5) + 3;
            depth *= 0.5;
            width *= 0.5;
            int t = r.nextInt(33) + 32;
            t *= 0.5;
            float dd = depth / t;
            float dw = width / t;
            for (int len = t * 2; len > 0; len--) {
                for (int h = 0; h < depth; h++)
                    for (int x = (int) -width; x <= width; x++)
                        for (int y = (int) -width; y <= width; y++)
                            for (int z = (int) -width; z <= width; z++) {
                                Location center = l.clone().add(x, y - h, z);
                                if (center.distanceSquared(l.clone().add(0, -h, 0)) < width * width && center.getBlock()
                                        .getType().equals(Material.END_STONE))
                                    center.getBlock().setType(Material.AIR);
                            }
                l.add(vel);
                vel.add(delta.clone().multiply(0.01));
                if (vel.length() > 2)
                    vel.multiply(0.5);
                if (len % 3 == 0)
                    depth += r.nextInt(3) - 1;
                if (len > t) {
                    depth += dd;
                    width += dw;
                } else {
                    depth -= dd;
                    width -= dw;
                }
            }
        }

    }
}