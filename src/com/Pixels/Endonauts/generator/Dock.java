package com.Pixels.Endonauts.generator;

import com.Pixels.Endonauts.StructureIO;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.Random;

public class Dock implements Generator {

    @Override
    public void generate(Random r, World w) {
        Location l = new Location(w, 0, 150, 0);
        while (!isDockLocation(l.clone())) {
            l = Structure.createLoc(r, w, 150);
            l.setY(56);
        }
        Location start = l.clone();
        if (Math.abs(l.getBlockX()) > Math.abs(l.getBlockZ())) {
            int move = 5;
            if (l.getBlockX() > 0)
                move *= -1;
            while (!(l.getBlockX() > -6 && l.getBlockX() < 6) && l.getBlock().getType().equals(Material.AIR)) {
                l.add(move, 0, 0);
                StructureIO.buildStructure(l.clone().add(0, -1, 0), "dock_ew",
                        new Material[]{Material.AIR});
            }
        } else {
            int move = 5;
            if (l.getBlockZ() > 0)
                move *= -1;
            while (!(l.getBlockZ() > -6 && l.getBlockZ() < 6) && l.getBlock().getType().equals(Material.AIR)) {
                l.add(0, 0, move);
                StructureIO.buildStructure(l.clone().add(0, -1, 0), "dock_ns",
                        new Material[]{Material.AIR});
            }
        }
        start.add(0, -1, 0);
        StructureIO.buildStructure(start, "dock_intersection", new Material[]{Material.AIR});
        buildDock(start.clone(), true, 5, r.nextInt(4) * 4);
        buildDock(start.clone(), false, 5, r.nextInt(4) * 4);
        buildDock(start.clone(), true, -5, r.nextInt(4) * 4);
        buildDock(start.clone(), false, -5, r.nextInt(4) * 4);
    }

    private void buildDock(Location l, boolean dir, int move, int length) {
        for (; length > 0; length--) {
            if (!l.getBlock().getType().equals(Material.AIR) || !l.clone().add(0, -1, 0).getBlock().getType()
                    .equals(Material.AIR))
                return;
            if (dir) {
                l.add(move, 0, 0);
                StructureIO.buildStructure(l.clone().add(0, 0, 0), "dock_ew",
                        new Material[]{Material.AIR});
            } else {
                l.add(0, 0, move);
                StructureIO.buildStructure(l.clone().add(0, 0, 0), "dock_ns",
                        new Material[]{Material.AIR});
            }
        }
    }

    private boolean isDockLocation(Location l) {
        if (!Structure.isVoid(l))
            return false;
        int ew = 0;
        int ns = 0;
        if (Math.abs(l.getBlockX()) > Math.abs(l.getBlockZ())) {
            ew = 1;
            if (l.getBlockX() > 0)
                ew = -1;
        } else {
            ns = 1;
            if (l.getBlockZ() > 0)
                ns = -1;
        }
        while (l.getBlockZ() != 0 && l.getBlockX() != 0) {
            l.setY(56);
            if (l.getBlock().getType().equals(Material.END_STONE))
                return true;
            l.add(ew, 0, ns);

        }
        return false;
    }
}