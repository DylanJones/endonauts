package com.Pixels.Endonauts.generator;

import com.Pixels.Endonauts.StructureIO;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Random;

public class Fleet implements Generator {

    @Override
    public void generate(Random r, World w) {
        for (int count = r.nextInt(9) + 8; count > 0; count--) {
            Location l = Structure.createLoc(r, w);
            l.setY(r.nextInt(128) + 88);
            StructureIO.buildStructure(l, "endship");
        }

    }
}