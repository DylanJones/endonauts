package com.Pixels.Endonauts.generator;

import com.Pixels.Endonauts.Globals;
import com.Pixels.Endonauts.StructureIO;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.EndGateway;

import java.util.Random;

public class Structure {
    public static Location createLoc(Random r, World w) {
        return createLoc(r, w, 100);
    }

    public static Location createLoc(Random r, World w, int radius) {
        return new Location(w, r.nextInt(radius * 2) - radius, r.nextInt(80), r.nextInt(radius * 2) - radius);
    }

    //Creates location at island edge, at y=56
    public static Location createShoreLoc(Random r, World w) {
        float angle = (float) (360 * r.nextDouble()); //YAW
        Location loc = new Location(w, 0, 56, 0, angle, 0);
        while (!loc.getBlock().getType().equals(Material.AIR))
            loc.add(loc.getDirection());
        loc.setYaw((angle + 180) % 360);
        return loc;
    }

    public static Location createEdgeLoc(Random r, World w) {
        // center is away from top, 100 away from edge
        double islandAngle = (Math.atan(35 / 100.0) / Math.PI * 180);
        // double islandAngle = 90;
        float pitchAngle = (float) (2 * islandAngle * r.nextDouble() - islandAngle);
        float yawAngle = (float) (r.nextDouble() * 360);
        Location loc = new Location(w, 0, 40, 0, yawAngle, pitchAngle);
        while (!loc.getBlock().getType().equals(Material.AIR)) {
            loc.add(loc.getDirection().multiply(5));
        }
        loc.setPitch(loc.getPitch() * -1);
        float yaw = loc.getYaw();
        yaw = (yaw + 180) % 360;
        loc.setYaw(yaw);
        return loc;
    }

    public static Location createGroundLoc(Random r, World w) {
        Location l = new Location(w, r.nextInt(200) - 100, r.nextInt(80), r.nextInt(200) - 100);
        while (!isGround(l))
            l = new Location(w, r.nextInt(200) - 100, r.nextInt(80), r.nextInt(200) - 100);
        l.setY(52D);
        while (!l.getBlock().getType().equals(Material.AIR))
            l.add(0, 1, 0);
        return l;
    }

    public static boolean isGround(Location l) {
        l = l.clone();
        l.setY(58D);
        return l.getBlock().getType().equals(Material.END_STONE);
    }

    public static Location fillDown(Material m, Location loc) {
        Location l = loc.clone();
        while (l.getY() > -1 && l.getBlock().getType().equals(Material.AIR)) {
            l.getBlock().setType(m);
            l.add(0, -1, 0);
        }
        return l;
    }

    public static boolean isVoid(Location l) {
        while (l.getBlockY() > -1) {
            if (!l.getBlock().getType().equals(Material.AIR))
                return false;
            l.add(0, -1, 0);
        }
        return true;
    }

    public static void fillArea(Location start, Location end, Material m) {
        Location min = new Location(start.getWorld(), Math.min(start.getBlockX(), end.getBlockX()),
                Math.min(start.getBlockY(), end.getBlockY()), Math.min(start.getBlockZ(), end.getBlockZ()));
        Location max = new Location(start.getWorld(), Math.max(start.getBlockX(), end.getBlockX()),
                Math.max(start.getBlockY(), end.getBlockY()), Math.max(start.getBlockZ(), end.getBlockZ()));
        Location l = min.clone();
        while (l.getBlockY() <= max.getBlockY()) {
            while (l.getBlockZ() <= max.getBlockZ()) {
                while (l.getBlockX() <= max.getBlockX()) {
                    Block b = l.getBlock();
                    b.setType(m);
                    l.setX(l.getX() + 1);
                }
                l.setX(min.getX());
                l.setZ(l.getZ() + 1);
            }
            l.setZ(min.getZ());
            l.setY(l.getY() + 1);
        }
    }

    public static void removeGateways() {
        Location l = new Location(Globals.gameWorld, 0, 75, 0);
        for (int angle = 0; angle < 360; angle += 18) {
            int x = (int) (Math.cos(Math.toRadians(angle)) * 96);
            int z = (int) (Math.sin(Math.toRadians(angle)) * 96);
            l.setX(x);
            l.setZ(z);
            StructureIO.buildStructure(l.clone().add(-1, -2, -1), "gateway");
            EndGateway e = (EndGateway) l.getBlock().getState();
            e.setExactTeleport(true);
            e.setExitLocation(l.clone().add(0, 64, 0)); // TODO fix gateways
        }
    }
}