package com.Pixels.Endonauts.generator;

import org.bukkit.World;

import java.util.Random;

public interface Generator {
    void generate(Random r, World w);
}