package com.Pixels.Endonauts.generator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

import java.util.Random;

public class Crystals implements Generator {
    @Override
    public void generate(Random r, World w) {
        for (int count = r.nextInt(9) + 8; count > 0; count--) {
            Location l = Structure.createGroundLoc(r, w);
            l.add(0, r.nextInt(17) + 8, 0);
            l.clone().add(0, 1, 0).getBlock().setType(Material.BEDROCK);
            float rad = r.nextInt(3) + 1.75F;
            for (int x = (int) -rad; x <= rad; x++)
                for (int z = (int) -rad; z <= rad; z++) {
                    if (l.clone().add(x, 0, z).distanceSquared(l) < rad * rad) {
                        Structure.fillDown(Material.OBSIDIAN, l.clone().add(x, 0, z));
                    }
                }
            l.add(0.5, 2, 0.5);
            w.spawnEntity(l, EntityType.ENDER_CRYSTAL);
        }
    }
}