package com.Pixels.Endonauts.generator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.Random;

public class Cave implements Generator {
    @Override
    public void generate(Random r, World w) {
        for (int count = r.nextInt(2) + 2; count > 0; count--) {
            Location start;
            do {
                start = Structure.createEdgeLoc(r, w);
            }
            while (start.getY() < 35);
            generateCave(start, 0.01, w, r);
        }
    }

    private void generateCave(Location start, double splitProb, World w, Random r) {
        Vector accel = new Vector(r.nextGaussian() * 2 - 1, r.nextGaussian() * 2 - 1, r.nextGaussian() * 2 - 1);
        // Vector velocity = new Vector(r.nextInt(3) - 1, r.nextInt(3) - 1,
        // r.nextInt(3) - 1);
        Vector velocity = start.getDirection();
        Location currentPos = start.clone();
        int airEncounters = 0;
        for (int len = r.nextInt(64) + 64; len > 0; len--) {
            // Cave
            int size = r.nextInt(4) + 4;
            boolean foundEndstone = false;
            for (int x = -size; x <= size; x++)
                for (int y = -size; y <= size; y++)
                    for (int z = -size; z <= size; z++) {
                        Location center = currentPos.clone().add(x, y, z);
                        if (center.distanceSquared(currentPos) < size * size /*
                         * && center.getBlock().getType().equals
                         * (Material.ENDER_STONE)
                         */)
                            center.getBlock().setType(Material.AIR);
                        if (center.getBlock().getType().equals(Material.END_STONE))
                            foundEndstone = true;
                    }
            // The air check
            if (!foundEndstone) {
                airEncounters++;
            } else {
                airEncounters = 0;
            }
            if (airEncounters > 4) {
                return;
            }
            // currentPos.getBlock().setType(Material.GLASS);
            // Move
            currentPos.add(velocity);
            velocity.add(accel.clone().multiply(0.005));
            if (len % 16 == 0)
                accel = new Vector(r.nextGaussian() * 2 - 1, r.nextGaussian() * 2 - 1, r.nextGaussian() * 2 - 1);
            if (velocity.length() > 2)
                velocity.multiply(0.5);
            if (len == 1 && currentPos.getBlockY() > 0 && currentPos.getBlockY() < 70)
                if (currentPos.getBlockX() > -100 && currentPos.getBlockX() < 100 && currentPos.getBlockZ() > -100
                        && currentPos.getBlockZ() < 100)
                    len = 2;

            splitProb += 0.0001;
            if (r.nextDouble() < splitProb) {
                splitProb = 0.001;
                float newpitch = (float) (currentPos.getPitch() + r.nextDouble() * 90 - 45);
                float newyaw = (float) (currentPos.getYaw() + (r.nextDouble() * 180 - 90));
                if (newpitch > 90) {
                    newyaw = -newyaw;
                    newpitch = 90 - (newpitch - 90);
                } else if (newpitch < -90) {
                    newyaw = -newyaw;
                    newpitch = 90 + (newpitch + 90);
                }
                Location newLoc = currentPos.clone();
                newLoc.setYaw(newyaw);
                newLoc.setPitch(newpitch);
                generateCave(newLoc, 0.001, w, r);
            }
        }
    }
}