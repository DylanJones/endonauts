package com.Pixels.Endonauts.generator;

import com.Pixels.Endonauts.StructureIO;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.Random;

public class Mine implements Generator {
    private final Material[] replace = {Material.END_STONE, Material.OBSIDIAN};

    @Override
    public void generate(Random r, World w) {
        int alt = 48;
        for (int count = r.nextInt(3) + 1; count > 0; count--) {
            Location l = Structure.createGroundLoc(r, w);
            l.setY(alt);
            Location l2 = l.clone().add(0, 0, -2);
            while (!l.getBlock().getType().equals(Material.AIR) || l.getBlockZ() < 150) {
                l.add(0, 0, 7);
            }
            StructureIO.buildStructure(l, "mineshaft", replace);
            while ((!l.getBlock().getType().equals(Material.AIR) || l.getZ() > -150) && l.getZ() > -1000) {
                StructureIO.buildStructure(l, "mineshaft", replace);
                l.add(0, 0, -7);
            }
            //
            while (!l2.getBlock().getType().equals(Material.AIR) || l2.getBlockX() < 150) {
                l2.add(7, 0, 0);
            }
            StructureIO.buildStructure(l2, "mineshaft2", replace);
            while ((!l2.getBlock().getType().equals(Material.AIR) || l2.getX() > -150) && l2.getX() > -1000) {
                StructureIO.buildStructure(l2, "mineshaft2", replace);
                l2.add(-7, 0, 0);
            }

            alt -= 10;
        }

    }
}