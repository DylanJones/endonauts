package com.Pixels.Endonauts.generator;

import javax.swing.text.html.HTML;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.zip.GZIPInputStream;

enum TagType {
    TAG_END, TAG_BYTE, TAG_SHORT, TAG_INT, TAG_LONG, TAG_FLOAT, TAG_DOUBLE, TAG_BYTE_ARRAY, TAG_STRING, TAG_LIST,
    TAG_COMPOUND, TAG_INT_ARRAY, TAG_LONG_ARRAY;

    public static TagType fromNum(int num) {
        return TagType.values()[num];
    }
}

public class NbtParser {
    public static HashMap fromFile(InputStream f) throws IOException {
        GZIPInputStream in = new GZIPInputStream(f);
        TagType type = TagType.fromNum(in.read());
        HashMap data = new HashMap();
        decode(in, type, data, true);
        if (data.containsKey("")) {
            return (HashMap) data.get("");
        }
        return data;
    }

    private static short readShort(InputStream stream) throws IOException {
        return (short)(stream.read() << 8 | stream.read());
    }

    private static int readInt(InputStream stream) throws IOException {
        return readShort(stream) << 16 | readShort(stream);
    }

    private static long readLong(InputStream stream) throws IOException {
        return (((long)readInt(stream)) << 32) | ((long)readInt(stream));
    }


    public static boolean decode(InputStream f, TagType type, HashMap data, boolean named) throws IOException {
        StringBuilder titleB = new StringBuilder();
        if (type != TagType.TAG_END && named) {
            int len = readShort(f); // big endian? little endian now?
            for (int i = 0; i < len; i++) {
                titleB.append((char) f.read());
            }
        }
        String title = titleB.toString();
        switch (type) {
            case TAG_END:
                return false;
            case TAG_BYTE:
                data.put(title, (byte) f.read());
                break;
            case TAG_SHORT:
                data.put(title, readShort(f));
                break;
            case TAG_INT:
                data.put(title, readInt(f));
                break;
            case TAG_LONG:
                data.put(title, readLong(f));
                break;
            case TAG_FLOAT:
                data.put(title, Float.intBitsToFloat(readInt(f)));
                break;
            case TAG_DOUBLE:
                data.put(title, Double.longBitsToDouble(readLong(f)));
                break;
            case TAG_BYTE_ARRAY:
                int length = readInt(f);
                ArrayList<Byte> arr = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    arr.add((byte) f.read());
                }
                data.put(title, arr);
                break;
            case TAG_STRING:
                length = readShort(f);
                StringBuilder b = new StringBuilder();
                for (int i = 0; i < length; i++) {
                    b.append((char) f.read());
                }
                data.put(title, b.toString());
                break;
            case TAG_LIST:
                TagType subType = TagType.fromNum(f.read());
                length = readInt(f);
                ArrayList list = new ArrayList();
                HashMap tmp = new HashMap();
                for (int i = 0; i < length; i++) {
                    decode(f, subType, tmp, false);
                    list.add(tmp.get(""));
                }
                data.put(title, list);
                break;
            case TAG_COMPOUND:
                HashMap map = new HashMap();
                while (decode(f, TagType.fromNum(f.read()), map, true)) {
                }
                data.put(title, map);
                break;
            case TAG_INT_ARRAY:
                length = readInt(f);
                ArrayList<Integer> iarr = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    iarr.add(readInt(f));
                }
                data.put(title, iarr);
                break;
            case TAG_LONG_ARRAY:
                length = readInt(f);
                ArrayList<Long> larr = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    larr.add(readLong(f));
                }
                data.put(title, larr);
                break;
            default:
                throw new RuntimeException("Invalid NBT detected");
        }
        return true;
    }

    public static String printNbt(Object val) {
        StringBuilder out = new StringBuilder();
        if (val instanceof String) {
            out.append("\"" + val + "\"");
        } else if (val instanceof ArrayList) {
            out.append("[");
            for (int i = 0; i < ((ArrayList) val).size(); i++) {
                out.append(printNbt(((ArrayList) val).get(i)));
                if (i < ((ArrayList) val).size() - 1) {
                    out.append(",");
                }
            }
            out.append("]");
        } else if (val instanceof HashMap) {
            out.append("{");
            Object[] keys = ((HashMap) val).keySet().toArray();
            for (int i = 0; i < keys.length; i++) {
                out.append(printNbt(keys[i]));
                out.append(":");
                out.append(printNbt(((HashMap) val).get(keys[i])));
                if (i < keys.length - 1) {
                    out.append(",");
                }
            }
            out.append("}");
        } else if (val instanceof Byte) {
            out.append((byte) val);
        } else if (val instanceof Character) {
            out.append((int) val);
        } else if (val instanceof Short) {
            out.append((short) val);
        } else if (val instanceof Integer) {
            out.append((int) val);
        } else if (val instanceof Long) {
            out.append((long) val);
        } else if (val instanceof Double) {
            out.append((double) val);
        } else if (val instanceof Float) {
            out.append((float) val);
        }
        return out.toString();
    }
}
