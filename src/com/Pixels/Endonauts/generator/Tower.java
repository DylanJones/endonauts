package com.Pixels.Endonauts.generator;

import com.Pixels.Endonauts.StructureIO;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.Random;

public class Tower implements Generator {
    @Override
    public void generate(Random r, World w) {
        Location loc = Structure.createShoreLoc(r, w);
        System.out.println("Generating tower at " + loc);
        loc.add(-6, 0, -6);
        Structure.fillArea(loc.clone().add(3, 1, 3), loc.clone().add(9, 1, 9), Material.AIR);
        Structure.fillArea(loc.clone().add(2, 0, 2), loc.clone().add(10, 0, 10), Material.PURPUR_BLOCK);
        StructureIO.buildStructure(loc, "lighthouse_base", new Material[]{Material.AIR, Material.END_STONE});
        loc.add(3, 5, 3);
        for (int x = r.nextInt(4) + 3; x > 0; x--) {
            StructureIO.buildStructure(loc, "lighthouse_tower", new Material[]{Material.AIR, Material.END_STONE});
            loc.add(0, 4, 0);
        }
        loc.add(-1, 0, -1);
        StructureIO.buildStructure(loc, "lighthouse_top", new Material[]{Material.AIR, Material.END_STONE});
    }
}