package com.Pixels.Endonauts;

import com.Pixels.Endonauts.generator.Structure;
import com.Pixels.Endonauts.projectile.BlockChanger;
import com.Pixels.Endonauts.projectile.ItemRing;
import com.Pixels.Endonauts.projectile.Projectile;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Serializer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.*;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValueAdapter;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

class Loop implements Runnable {
    public long time;
    private boolean dragon;

    public Loop() {
        time = 0;
        dragon = false;
    }

    private static void infoWeapons(Player p) {
        String[] lores = new String[]{"Fires a low-power explosive beam", "Fires a dense object very fast",
                "Fires a high-power explosive beam", "Leaves an explosive surprise",
                "Fires a targeted missile.  &Must have a selected target",
                "Anihilates a tunnel of blocks &OR [Left click] fires an explosive projectile",
                "Creates a BEAM of MATTER", "Conceals the user", "Vwoop"};
        for (int x = 0; x < 9; x++) {
            ItemStack i = new ItemStack(Globals.tools[x]);
            ItemMeta meta = i.getItemMeta();
            ArrayList<String> lore = new ArrayList<>();
            lore.add(ChatColor.RESET + "" + ChatColor.AQUA + lores[x].split("&")[0]);
            if (lores[x].split("&").length > 1)
                lore.add(ChatColor.RESET + "" + ChatColor.AQUA + lores[x].split("&")[1]);
            meta.setLore(lore);
            i.setItemMeta(meta);
            p.getInventory().setItem(x, i);
        }
    }

    public void run() {
        time++;
        Collection<? extends Player> players = Globals.main.getServer().getOnlinePlayers();
        for (Player p : players) {
            // Targeting
            doTargeting(p);
            // Misc tasks
            sanitizePlayer(p);
            // GLIDE BOOST
            glideBoost(p);
            // WEAPONS
            rechargeWeapons(p);

            if (p.getOpenInventory().getType().equals(InventoryType.SHULKER_BOX))
                infoWeapons(p);
        }

        // Tick projectiles
        projectileTick();
        // Remove items
        removeDroppedItems();

        if (Globals.isDebug)
            runDebugStuff(players);
        /* **************************
         * RUNNING GAME
         **********************************/
        switch (Globals.gameState) {
            case LOADING:
                // Remove Ender Dragon
                removeEnderDragons();
                break;
            case IDLE:
                break;
            case STARTING:
                GameStateChangeHandler.starting();
                break;
            case SETUP:
                GameStateChangeHandler.setup();
                break;
            case INGAME:
                // Spawn powerups4
                powerupSpawn();
                // Check for win condition
                checkWin();
                GameStateChangeHandler.ingame();
                //Make crystals connect to players
                enderCrystalBeams();
                break;
            case DEATHMATCH:
                GameStateChangeHandler.deathmatch();
                checkWin();
                enderCrystalBeams();
                break;
            case ENDGAME:
                GameStateChangeHandler.endgame();
        }

    }

    private void enderCrystalBeams() {
        if (Globals.players.size() >= 1) {
            for (EnderCrystal e : Globals.gameWorld.getEntitiesByClass(EnderCrystal.class)) {
                Player closest = null;
                double d = Double.POSITIVE_INFINITY;
                for (Player p : Globals.players) {
                    if (p.getLocation().distanceSquared(e.getLocation()) < d && !p.getGameMode()
                            .equals(GameMode.SPECTATOR)) {
                        d = p.getLocation().distanceSquared(e.getLocation());
                        closest = p;
                    }
                }
                if (closest == null)
                    return;
                if (e.getLocation().distanceSquared(closest.getLocation()) < 32 * 32) {
                    e.setBeamTarget(closest.getLocation().clone().add(0, -1, 0));
                } else {
                    e.setBeamTarget(null);
                }
            }
        }
    }

    private void powerupSpawn() {
        int count = 0;
        for (Projectile p : Globals.projectiles) {
            if (p instanceof ItemRing) {
                count++;
            }
        }
//        System.out.println(Globals.projectiles.size());
        if (count < 8) {
            Location loc = new Location(Globals.gameWorld, Math.random() * 300 - 150, Math.random() * 128,
                    Math.random() * 300 - 150, (float) (Math.random() * 360), 4);
            Globals.projectiles.add(new ItemRing(loc));
        }
    }

    private void checkWin() {
        if ((Globals.players.size() <= 1 && !Globals.isDebug) || (Globals.players.size() == 0)) {
            if (Globals.players.size() == 1)
                Utils.broadcastMessage(Globals.players.get(0).getName() + " won the game!");
            else
                Utils.broadcastMessage("It seems the game is over, but nobody won.  How odd.");
            Globals.startTimer = 200;
            for (Player p : Globals.gameWorld.getPlayers())
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
            Main.changeState(GameState.ENDGAME);
            sanitizeProjectiles();
            Globals.players = new ArrayList<>(Globals.main.getServer().getOnlinePlayers());
            Globals.deathmatchTimer = 0;
        }
    }

    private void rechargeWeapons(Player p) {
        ItemStack[] inv = p.getInventory().getContents();
        for (int x = 0; x < 9; x++) {
            if (inv[x] != null && !inv[x].getType().equals(Material.BLACK_DYE)) {
                Damageable meta = (Damageable) inv[x].getItemMeta();
                if (meta.getDamage() > 0) {
                    meta.setDamage(meta.getDamage() - Globals.recharge[x]);
                    inv[x].setItemMeta((ItemMeta) meta);
                }
            }
        }
    }

    private void removeDroppedItems() {
        if(Globals.gameWorld == null)
            return;
        for (Entity e : Globals.gameWorld.getEntities())
            if (e.getType().equals(EntityType.DROPPED_ITEM) || e.getType().equals(EntityType.EXPERIENCE_ORB))
                e.remove();
    }

    private void sanitizePlayer(Player p) {
        // Set food bar
        p.setFoodLevel(20);
        p.setSaturation(20);
        // ATTRIBUTES
        // p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(0);
        p.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(0);
        // Set elytra
        p.getInventory().setChestplate(new ItemStack(Material.ELYTRA));
        // Health Regen
/*        if (time % 25 == 0 && p.getHealth() + 1 < 20 && !p.isDead())
            p.setHealth(p.getHealth() + 1);*/

        // Reset inventory if in survival
        if (p.getGameMode().equals(GameMode.SURVIVAL))
            for (int x = 9; x < p.getInventory().getSize(); x++)
                // if there are buttons, remove them
                if (p.getInventory().getItem(x) != null && p.getInventory().getItem(x).getType()
                        .equals(Material.LEGACY_STAINED_GLASS_PANE)) { // TODO fix the check to not be deprecated
                    InventoryMenuHandler.clearButtons(p);
                    break;
                }

        // Set weapon name
        if (p.getGameMode().equals(GameMode.SURVIVAL))
            for (int x = 0; x < 9; x++) {
                ItemStack i = p.getInventory().getItem(x);
                if (i != null) {
                    ItemMeta im = i.getItemMeta();
                    if (!im.equals(i.getItemMeta()))
                        i.setItemMeta(im);
                }
            }
    }

    private void glideBoost(Player p) {
        Location center = new Location(p.getWorld(), 0, 64, 0);
        if (p.getExp() < 0)
            p.setExp(0);
        else if (p.getExp() > 1)
            p.setExp(1);
        if (p.isGliding() && p.isSneaking() && p.getExp() - 0.0075F > 0.0) {
            p.setVelocity(p.getVelocity().add(p.getLocation().getDirection().multiply(0.05)));
            p.setExp((p.getExp() - 0.0075F));
            if (p.getPotionEffect(PotionEffectType.INVISIBILITY) == null)
                p.getLocation().getWorld().spawnParticle(Particle.END_ROD, p.getLocation(), 0);
        } else if (p.getExp() + 0.002 < 1.0 && !p.isOnGround() && p.getLocation().distanceSquared(center) < 250000) {
            p.setExp((p.getExp() + 0.002F));
        }
        if (p.isOnGround() && p.getExp() + 0.015 < 1.0 && p.getExp() + 0.015 > 0
                && p.getLocation().distanceSquared(center) < 250000)
            p.setExp((p.getExp() + 0.015F));
        if (p.getExpToLevel() >= 1)
            p.setLevel(0);

        if (p.getLocation().distanceSquared(center) > 562500) {
            p.setExp(0);
            p.setVelocity(p.getVelocity().multiply(0.75));
            p.getLocation().add(0.0, 0.5, 0.0);
        }
    }

    private void removeEnderDragons() {
        for (Entity e : Globals.gameWorld.getEntities())
            if (e instanceof EnderDragon && !dragon) {
                ((EnderDragon) e).setHealth(0);
                dragon = true;
                Globals.main.getServer().getScheduler().runTaskLater(Globals.main, () -> {
                    Structure.fillArea(new Location(Globals.gameWorld, -3, 199, -3),
                            new Location(Globals.gameWorld, 3, 204, 3), Material.AIR);
                    for (Player p : Globals.gameWorld.getPlayers())
                        p.stopSound("entity.ender_dragon.death");
                }, 1);
                Globals.main.getServer().getScheduler().runTaskLater(Globals.main, () -> {
                    Structure.removeGateways();
                    dragon = false;
                    Main.changeState(GameState.IDLE);
                    Globals.main.getServer().getScheduler().runTaskLater(Globals.main, () -> Structure
                            .fillArea(new Location(Globals.gameWorld, -3, 199, -3),
                                    new Location(Globals.gameWorld, 3, 204, 3), Material.AIR), 5);
                }, 201);
            }
    }

    private void doTargeting(Player src) {
        Location loc = src.getEyeLocation();
        String target = null;
        Player p2 = null;
        Vector increment = loc.getDirection();
        for (int x = 0; x < 125; x++) {
            loc.add(increment);
            // p.getWorld().spawnParticle(Particle.FLAME, loc, 0);
            for (Player p1 : src.getWorld().getPlayers()) {
                if (p1.getLocation().distanceSquared(loc) <= x / 4.0 + 2 && p1 != src) {
                    target = p1.getName();
                    p2 = p1;
                }
            }
        }
        if(src.getGameMode().equals(GameMode.SPECTATOR))
            target = null;
        
        if (target != null && ((!src.hasMetadata("lookingAt")) || !src.getMetadata("lookingAt").get(0).asString()
                .equals(target))) {
            final String str = target;
            src.setMetadata("lookingAt", new MetadataValueAdapter(Globals.main) {
                @Override
                public Object value() {
                    return str;
                }

                @Override
                public void invalidate() {
                }

                public String toString() {
                    return str;
                }
            });
            sendPacket(src, p2, true);
        } else if (target == null) {
            if (src.hasMetadata("lookingAt")) {
                Player g = src.getServer().getPlayer(src.getMetadata("lookingAt").get(0).asString());
                if (g != null) {
                    src.removeMetadata("lookingAt", Globals.main);
                    sendPacket(src, g, false);
                }
            }
        }
    }

    private void projectileTick() {
        // Tick Projectiles
        Iterator<Projectile> i = Globals.projectiles.iterator();
        while (i.hasNext()) {
            try {
                if (i.next().tick()) {
                    i.remove();
                }
            } catch (Exception e) {
                i.remove();
            }
        }
    }

    private void sendPacket(Player dest, Player glower, boolean glow) {
        ProtocolManager pm = ProtocolLibrary.getProtocolManager();
        PacketContainer packet = pm.createPacket(PacketType.Play.Server.ENTITY_METADATA);
        packet.getIntegers().write(0, glower.getEntityId());
        WrappedDataWatcher watcher = new WrappedDataWatcher();
        Serializer serializer = Registry.get(Byte.class);
        watcher.setEntity(glower);
        byte stat = getStatus(glower);
        if (glow)
            stat |= 0x40;
        else
            stat &= ~0x40;
        watcher.setObject(0, serializer, stat);
        packet.getWatchableCollectionModifier().write(0, watcher.getWatchableObjects());
        try {
            pm.sendServerPacket(dest, packet);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private byte getStatus(Player p) {
        byte out = 0x0;
        if (p.getFireTicks() > 0)
            out |= 0x1;
        if (p.isSneaking())
            out |= 0x2;
        if (p.isSprinting())
            out |= 0x8;
        if (p.isGlowing())
            out |= 0x40;
        if (p.isGliding())
            out |= 0x80;
        return out;
    }

    private void sanitizeProjectiles() {
        Globals.projectiles.removeIf(projectile -> !(projectile instanceof BlockChanger));
    }

    private void runDebugStuff(Collection<? extends Player> players) {
        for (Player p : players) {
            p.setExp(0.99F);
            for (int x = 0; x < 9; x++) {
                ItemStack is = new ItemStack(Globals.tools[x]);
                if (!is.equals(p.getInventory().getItem(x)))
                    p.getInventory().setItem(x, is);

            }
        }
    }
}