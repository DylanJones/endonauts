package com.Pixels.Endonauts;

import com.Pixels.Endonauts.generator.*;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.Random;

public class WorldStructureGenerator {
    public static final Generator[] generators = new Generator[]{new Cave(), new Ravine(), new Fleet(), new Tower(),
            new Mine(), new Crystals(), new Dock()};

    public static void generate(long seed, World w) {
        System.out.println("Generator Called.....");
        w.getBlockAt(0, 200, 0).setType(Material.END_STONE);
        Random r = new Random(seed);
        for (Generator g : generators) {
            if (r.nextInt(generators.length / 2) == 0) {
                System.out.println("GENERATING" + g);
                g.generate(r, w);
            }
        }
        System.out.println("Done!");
    }

}
