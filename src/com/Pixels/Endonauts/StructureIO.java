package com.Pixels.Endonauts;

import com.Pixels.Endonauts.generator.NbtParser;
import org.bukkit.Axis;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.Orientable;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;

import java.io.*;
import java.util.*;

public class StructureIO {
    public static void readToFile(Location start, Location end, String filename) throws IOException {
        readToFile(start, end, filename, new Material[0]);
    }

    public static void buildStructure(Location start, String filename) {
        buildStructure(start, filename, null);
    }

    //    @SuppressWarnings("deprecation")
    public static void readToFile(Location start, Location end, String filename,
                                  Material[] ignore) throws IOException {
        Location min = new Location(start.getWorld(), Math.min(start.getBlockX(), end.getBlockX()),
                Math.min(start.getBlockY(), end.getBlockY()), Math.min(start.getBlockZ(), end.getBlockZ()));
        Location max = new Location(start.getWorld(), Math.max(start.getBlockX(), end.getBlockX()),
                Math.max(start.getBlockY(), end.getBlockY()), Math.max(start.getBlockZ(), end.getBlockZ()));
        Location l = min.clone();
        ArrayList<Material> palette = new ArrayList<>();
        ArrayList<HashMap> blocks = new ArrayList<>();
        while (l.getBlockY() <= max.getBlockY()) {
            while (l.getBlockZ() <= max.getBlockZ()) {
                while (l.getBlockX() <= max.getBlockX()) {
                    Block b = l.getBlock();
                    if (Arrays.asList(ignore).contains(b.getType())) {
                        // Don't add TODO: should this add air or do nothing?
                    } else {
                        // check if the block is in the palette yet
                        int idx = -1;
                        for (int i = 0; i < palette.size(); i++) {
                            if (palette.get(i).equals(b.getType())) {
                                System.out.println("yes");
                                idx = i;
                            }
                        }
                        if (idx == -1) {
                            // block not in palette
                            palette.add(b.getType());
                            idx = palette.size() - 1;
                        }
                        HashMap loc = new HashMap();
                        loc.put("pos", Arrays.asList(l.getBlockX(), l.getBlockY(), l.getBlockZ()));
                        loc.put("state", idx);
                        blocks.add(loc);
                    }
                    l.setX(l.getX() + 1);
                }
                l.setX(min.getX());
                l.setZ(l.getZ() + 1);
            }
            l.setZ(min.getZ());
            l.setY(l.getY() + 1);
        }
        System.out.println();
        // TODO write nbt
    }

    public static void buildStructure(Location start, String filename,
                                      Material[] replace) {
        if (!buildStructureNbt(start, filename, replace)) {
            buildStructureCustom(start, filename, replace);
        }
    }

    public static boolean buildStructureCustom(Location start, String filename, Material[] replace) {
        Queue<String> s = new LinkedList<>();
        HashMap<Integer, Material> decodeMaterials = new HashMap<>();
        try {
            BufferedReader r = new BufferedReader(
                    new InputStreamReader(Main.class.getResource("/assets/" + filename + ".mcs").openStream()));
            while (r.ready()) {
                s.add(r.readLine());
            }
            r = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/assets/conversions.txt")));
            while (r.ready()) {
                String[] line = r.readLine().split(":");
                decodeMaterials.put(Integer.parseInt(line[1]), Material.valueOf(line[0]));
            }
        } catch (NullPointerException | IOException e) {
            System.err.println("Failed to load structure \"" + filename + "\"");
            return false;
        }
        // begin pasted code
        Location l = start.clone();
        while (!s.isEmpty()) {
            String input = s.poll();
            if (input.equals("/")) {
                l.add(0, 1, 0);
                l.setX(start.getX());
                l.setZ(start.getZ());
            } else {
                for (String n : input.split(" ")) {
                    if (!n.equals("")) {
                        String[] data = n.split(":");
                        if ((replace == null || Arrays.asList(replace).contains(l.getBlock().getType()))) {
                            l.getBlock().setType(decodeMaterials.get(Integer.parseInt(data[0])));
//                            l.getBlock().setData(Byte.parseByte(data[1]));
                        }
                        l.add(1, 0, 0);
                    }
                }
                l.add(0, 0, 1);
                l.setX(start.getX());
            }
        }
        // end pasted code
        // TODO save back to NBT file
        return true;
    }

    public static boolean buildStructureNbt(Location start, String filename, Material[] replace) {
        HashMap comp;
        try {
            comp = NbtParser.fromFile(Main.class.getResourceAsStream("/assets/" + filename + ".nbt"));
        } catch (IOException | NullPointerException e) {
            System.err.println("Failed to load structure \"" + filename + "\"");
            return false;
        }
        Set<Material> ignoreSet;
        Set<Material> replaceSet;
        if (replace != null) {
            replaceSet = new HashSet<>(Arrays.asList(replace));
        } else {
            replaceSet = null;
        }
//        System.out.println(NbtParser.printNbt(comp));
        ArrayList palette = (ArrayList) comp.get("palette");
        ArrayList blocks = (ArrayList) comp.get("blocks");
        for (Object b : blocks) {
            HashMap blockNbt = (HashMap) b;
            ArrayList<Integer> relpos = (ArrayList<Integer>) blockNbt.get("pos");
            HashMap data = (HashMap) palette.get((Integer) blockNbt.get("state"));
            Material blockType = Material.matchMaterial((String) data.get("Name"));
            if (blockType == null) {
                System.err.println("Unknown material " + data.get("Name"));
            }
            Location loc = start.clone().add(relpos.get(0), relpos.get(1), relpos.get(2));
            Block block = loc.getBlock();
            if (replaceSet == null || replaceSet.contains(block.getType())) {
                block.setType(blockType);
                // TODO blockstate
                if (data.containsKey("Properties")) {
                    // TODO properties/blockstate
                    HashMap props = (HashMap) data.get("Properties");
                    BlockData blockData = block.getBlockData();
                    for (Object prop : props.keySet()) {
                        switch ((String) prop) {
                            case "facing":
                                Directional dir = (Directional) blockData;
                                dir.setFacing(BlockFace.valueOf(((String) props.get(prop)).toUpperCase()));
                                break;
                            case "half":
                                if (blockData instanceof Bisected) {
                                    Bisected half = (Bisected) blockData;
                                    half.setHalf(Bisected.Half.valueOf(((String) props.get(prop)).toUpperCase()));
                                } else {
//                                    System.out.println("NO BISECT REEEE " + data);
//                                    System.out.println("NO BISECT REEEE " + blockNbt);
//                                    System.out.println("NO BISECT REEEE " + block);
//                                    System.out.println("NO BISECT REEEE " + blockData);
                                    ((Slab) blockData).setType(Slab.Type.valueOf(((String) props.get(prop)).toUpperCase()));
                                }
                                break;
                            case "shape":
                                Stairs stairs = (Stairs) blockData;
                                stairs.setShape(Stairs.Shape.valueOf(((String) props.get(prop)).toUpperCase()));
                                break;
                            case "axis":
                                Orientable orientable = (Orientable) blockData;
                                orientable.setAxis(Axis.valueOf(((String) props.get(prop)).toUpperCase()));
                                break;
                            default:
                                System.out.println("Unknown block attribute: " + prop);
                        }
                    }
                    block.setBlockData(blockData);
                }
            }
        }
        return true;
    }
}
