package com.Pixels.Endonauts;

import com.Pixels.Endonauts.projectile.*;
import com.Pixels.Endonauts.projectile.Projectile;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
//import org.bukkit.craftbukkit.v1_12_R1.entity.CraftArmorStand;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.HashSet;

class Observer implements Listener {
    public Observer() {
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e
                .getPlayer().getGameMode().equals(GameMode.SURVIVAL))) {
            if (e.getItem() == null || Arrays.asList(Globals.placeholders).contains(e.getItem().getType())) {
                return;
            }
            ItemMeta im = e.getItem().getItemMeta();
            if ((im instanceof Damageable && ((Damageable) im).getDamage() > 0) || e.getItem().getAmount() == 0) {
                return;
            }
            Location l = p.getEyeLocation();
            Location l2 = l.clone();

            if (p.isGliding() && !p.isSneaking())
                l.add(0, -1.25, 0);

            int slot = p.getInventory().getHeldItemSlot();
            // TODO: TEMPORARY
            if (slot != 0)
                e.getItem().setAmount(e.getItem().getAmount() - 1);

            Player target = null;
            if (p.hasMetadata("lookingAt"))
                target = Globals.main.getServer().getPlayer(p.getMetadata("lookingAt").get(0).asString());
            ((Damageable) im).setDamage(Globals.charge[slot]);
            e.getItem().setItemMeta(im);
            // ###########
            switch (slot) {
                // Small Fireworks Beam
                case 0:
                    Globals.projectiles.add(new FireworksBeam(l, l2, p));
                    break;
                // Arrow
                case 1:
                    Arrow a = l.getWorld().spawnArrow(l.add(l.getDirection().multiply(4)), l.getDirection(),
                            (float) (p.getVelocity().length() + 4f), 0F);
                    a.setShooter(p);
                    a.setCustomName("1-" + p.getName());
                    break;
                // Large Fireworks Beam
                case 2:
                    Globals.projectiles.add(new FireworksBeamII(l, l2, p));
                    break;
                // MINE
                case 3:
                    Globals.projectiles.add(new Mine(p, l));
                    break;
                // Rocket
                case 4:
                    if (target != null) {
                        Globals.projectiles.add(new Missile(target, p.getName(), l));
                    } else {
                        // Make it like they didn't use up the item
                        p.getInventory().getItemInMainHand()
                                .setAmount(p.getInventory().getItemInMainHand().getAmount() + 1);
//                        p.getInventory().getItemInMainHand().setDurability((short) 0);
                        ((Damageable)im).setDamage(0);
                        p.getInventory().getItemInMainHand().setItemMeta(im);
                    }
                    break;
                // Antimatter Beam
                case 5:
                    AntimatterBeam ab = new AntimatterBeam(
                            p.getEyeLocation().clone().add(p.getEyeLocation().getDirection().multiply(3)));
                    Globals.projectiles.add(ab);
                    break;
                // Matter Beam
                case 6:
                    MatterBeam mb = new MatterBeam(p.getName(),
                            p.getEyeLocation().add(p.getEyeLocation().getDirection().multiply(2)));
                    Globals.projectiles.add(mb);
                    break;
                // Chaff
                case 7:
                    for (int n = 0; n < 16; n++) {
                        Globals.projectiles.add(new Chaff(l));
                    }
                    for (Projectile proj : Globals.projectiles) {
                        if (proj instanceof Missile) {
                            Player t = (Player) ((Missile) proj).target;
                            if (t.equals(p)) {
                                ((Missile) proj).target = null;
                            }
                        }
                    }
                    p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 160, 0), true);
                    for (Player otherPlayer : Globals.gameWorld.getPlayers()) {
                        if (otherPlayer != p) {
                            otherPlayer.hidePlayer(Main.getPlugin(Main.class), p);
                        }
                    }
                    Globals.main.getServer().getScheduler().runTaskLater(Globals.main, () -> {
                        for (Player player : Globals.gameWorld.getPlayers()) {
                            player.showPlayer(Main.getPlugin(Main.class), p);
                        }
                    }, 160);
                    break;
                // Teleport
                case 8:
                    Globals.projectiles.add(new PlayerTeleport(l, l2, p));
                    break;
                // ######
            }
            if (e.getItem().getAmount() == 0) {
                p.getInventory().setItem(slot, (new ItemStack(Globals.placeholders[slot], 1)));
//                p.getInventory().getItem(slot).setDurability((short) slot); // TODO reimplement this
            }

        } else if (e.getAction().equals(Action.LEFT_CLICK_BLOCK) && e.getClickedBlock().getType()
                .equals(Material.PURPLE_STAINED_GLASS)) {
            e.getClickedBlock().breakNaturally();
        } else if ((e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK))) {
            // Left click of Antimatter Beam
            if (p.getInventory().getHeldItemSlot() == 5) {
                if (e.getItem() == null || e.getItem().getType().equals(Material.BLACK_DYE)) {
                    return;
                }
                ItemMeta im = e.getItem().getItemMeta();
                if ((im instanceof Damageable && ((Damageable) im).getDamage() > 0) || e.getItem().getAmount() == 0) {
                    return;
                }
                ExplodeyBeam eb = new ExplodeyBeam(
                        p.getEyeLocation().clone().add(p.getEyeLocation().getDirection().multiply(3)), p);
                Globals.projectiles.add(eb);
                e.getItem().setAmount(e.getItem().getAmount() - 1);
                ((Damageable)im).setDamage(Globals.charge[5]);
                e.getItem().setItemMeta(im);
                if (e.getItem().getAmount() == 0) {
                    p.getInventory().setItem(5, (new ItemStack(Material.BLACK_DYE, 1)));
//                    p.getInventory().getItem(5).setDurability((short) 5); // TODO reimplement this
                }
            }
        }
    }

    // CUSTOM DEATH MESSAGES
    @EventHandler
    private void onDeath(PlayerDeathEvent e) {
        Globals.players.remove(e.getEntity());
        e.getEntity().spigot().respawn();
        e.getEntity().setGameMode(GameMode.SPECTATOR);
        e.getEntity().getInventory().clear();
        InventoryMenuHandler.updatePlayerMenu(e.getEntity(), false);

        if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent edee = (EntityDamageByEntityEvent) (e.getEntity().getLastDamageCause());
            if (edee.getDamager().getCustomName() != null)
                e.setDeathMessage(
                        e.getEntity().getName() + DeathMessageHandler.handleMessage(edee.getDamager().getCustomName()));
        }
        // Matter Beam death messages
        if (e.getEntity().getLastDamageCause().getCause().equals(DamageCause.FLY_INTO_WALL) || e.getEntity()
                .getLastDamageCause().getCause().equals(DamageCause.FALL)) {
            Block b = e.getEntity().getLocation().clone().add(e.getEntity().getLocation().getDirection()).getBlock();
            for (Projectile p : Globals.projectiles) {
                if (p instanceof MatterBeam) {
                    final int SQUARE_RADIUS = 2;
                    for (int x = 0; x < SQUARE_RADIUS; x++) {
                        for (int y = 0; y < SQUARE_RADIUS; y++) {
                            for (int z = 0; z < SQUARE_RADIUS; z++) {
                                Block tar = b.getLocation()
                                        .add(new Vector((SQUARE_RADIUS / 2) - x, (SQUARE_RADIUS / 2) - y,
                                                (SQUARE_RADIUS / 2) - z)).getBlock();
                                if (((MatterBeam) p).containsBlock(tar)) {
                                    e.setDeathMessage(e.getEntity().getName() + DeathMessageHandler
                                            .handleMessage(((MatterBeam) p).getMessage()));
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void arrowDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager().getCustomName() != null)
            if (e.getDamager().getCustomName().split("-")[0].equals("1") && e.getDamager().getCustomName().split("-")[1]
                    .equals(e.getEntity().getName()))
                e.setDamage(0);
    }

    @EventHandler
    public void clientPing(ServerListPingEvent e) {
        e.setServerIcon(Globals.ICON);
    }

    // PLAYER LOGIN
    @EventHandler
    public void playerLogin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.teleport(new Location(Globals.gameWorld, 0.5, 150, 0.5));
        p.setBedSpawnLocation(new Location(Globals.gameWorld, 0, 150, 0), true);

        if (!(Globals.gameState == GameState.INGAME || Globals.gameState == GameState.DEATHMATCH))
            Globals.players.add(p);

        p.setGameMode(GameMode.SPECTATOR);
        p.getInventory().clear();
        InventoryMenuHandler.updatePlayerMenu(p, false);
        p.sendMessage(Globals.chatName + "Welcome!  Open your inventory to access the menu.");
    }

    @EventHandler
    public void playerLogout(PlayerQuitEvent e) {
        Globals.players.remove(e.getPlayer());
    }

    // Make ender crystals damage players if they are tracking a player
    @EventHandler
    public void entityExplode(EntityExplodeEvent e) {
        // Code to damage player when a crystal that they are connected to explodes
        if (e.getEntity().getType().equals(EntityType.ENDER_CRYSTAL)) {
            e.setYield(0); // explosions shouldn't break blocks
            Globals.projectiles.add(new CrystalRespawn(e.getEntity().getLocation()));
            Location target = ((EnderCrystal) e.getEntity()).getBeamTarget();
            if (target == null)
                return;
            Player closest = Globals.players.get(0);
            double d = Integer.MAX_VALUE;
            for (Player p : Globals.players) {
                if (p.getLocation().distanceSquared(target) < d) {
                    d = p.getLocation().distanceSquared(target);
                    closest = p;
                }
            }
            // Get the player that shot the ender crystal
            EntityDamageEvent lastDamageCause = e.getEntity().getLastDamageCause();
            String damagerName = Globals.isDebug ? "ender crystal player error" : "unknown";
            if (lastDamageCause instanceof EntityDamageByEntityEvent) { // shot by railgun
                EntityDamageByEntityEvent damagerEvent = (EntityDamageByEntityEvent) lastDamageCause;
                Entity damager = damagerEvent.getDamager();
                if (damager instanceof Player) {
                    damagerName = ((Player) damager).getDisplayName();
                } else {
                    try { // If the ender crystal was not killed by a player, then it was killed by one of the weapons
                        damagerName = damager.getCustomName().split("-")[1];
                    } catch (ArrayIndexOutOfBoundsException urbad) {
                        damagerName = "unknown";
                    }
                }
            }
            Entity armor = closest.getWorld().spawnEntity(new Location(closest.getWorld(), 0, 943432, 0),
                    EntityType.ARMOR_STAND);
            ((ArmorStand) armor).setVisible(false);
            armor.teleport(target);
            armor.setCustomName("8-" + damagerName);
            closest.damage(Math.random() * 4 + 8, armor);
            armor.remove();
        }
    }

    // BLOCKS
    @EventHandler
    public void breakBlock(BlockBreakEvent e) {
        if (e.getPlayer().getGameMode().equals(GameMode.SURVIVAL))
            e.setCancelled(true);
    }

    // TELEPORTS
    @EventHandler
    public void playerTeleport(PlayerTeleportEvent e) {
        // End Gateways just teleport the player 100 blocks up
        if (e.getCause().equals(TeleportCause.END_GATEWAY)) {
            Location destination = e.getFrom();
            destination.add(0, 100, 0);
            e.setTo(destination);
        }
    }
}
