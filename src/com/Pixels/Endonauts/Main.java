package com.Pixels.Endonauts;

import com.Pixels.Endonauts.generator.Tower;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

public class Main extends JavaPlugin {
    static final HashMap<Player, Data> TEMPDATA = new HashMap<>();// TEMPORARY

    public static void changeState(GameState newState) {
        Globals.gameState = newState;
        for (Player p : Globals.gameWorld.getPlayers())
            InventoryMenuHandler.updatePlayerMenu(p, false);
    }

    public void onEnable() {
        Globals.main = this;
        BukkitScheduler scheduler = this.getServer().getScheduler();
        scheduler.runTaskTimer(this, new Loop(), 10, 1);
        getServer().getPluginManager().registerEvents(new Observer(), this);
        getServer().getPluginManager().registerEvents(new InventoryMenuHandler(), this);
        setup();

        getServer().getPluginManager().registerEvents(new TEMP(), this);// TEMPORARY

        try {
            Globals.ICON = getServer().loadServerIcon(ImageIO.read(Main.class.getResource("/assets/Endonauts.png")));
        } catch (Exception e) {
            getLogger().warning("Failed to load icon!");
        }
        ProtocolManager pm = ProtocolLibrary.getProtocolManager();
        pm.addPacketListener(new PacketInterceptor(this, PacketType.Play.Server.ENTITY_METADATA));
    }

    @Override
    public void onDisable() {
    }

    // COMMANDS FOR DEBUG (TEMPORARY)
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("go")) {
            if (args.length >= 1 && sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage("going to " + args[0]);
                p.sendMessage("" + getServer().getWorlds());

                p.teleport(new Location(getServer().getWorld(args[0]), 0, 150, 0));
                return true;
            } else {
                sender.sendMessage("Available Worlds: " + getServer().getWorlds());
                return false;
            }
        } else if (cmd.getName().equalsIgnoreCase("readysave"))// TEMPORARY
        {
            Player p = (Player) sender;
            TEMPDATA.put(p, new Data(args[0]));
        } else if (cmd.getName().equalsIgnoreCase("endLoad")) {
            if (args.length >= 1) {
                long seed = Long.parseLong(args[0]);
                WorldGenHandler.generateWorld(seed);
                return true;
            } else {
                sender.sendMessage("Usage: /endLoad <numeric seed>");
                return false;
            }
        }
        // Debug mode
        else if (cmd.getName().equalsIgnoreCase("dbg")) {
            if (Globals.isDebug) {
                Utils.broadcastMessage("not debug");
            } else {
                Utils.broadcastMessage("debug");
            }
            Globals.isDebug = !Globals.isDebug;
            new Tower().generate(new Random(), Globals.gameWorld);
            return true;
        } else if (cmd.getName().equalsIgnoreCase("loadstruct")) {
            StructureIO.buildStructure(((Player) sender).getLocation(), args[0]);
            return true;
        } else if (cmd.getName().equals("paquete")) {
            sender.sendMessage(((Player) sender).getMetadata("lookingAt").toString());
            return true;
        } else if (cmd.getName().equalsIgnoreCase("state")) {
            if (args.length > 0) {
                try {
                    Main.changeState(GameState.valueOf(args[0]));
                } catch (IllegalArgumentException e) {
                    sender.sendMessage("\"{{state}}\" is not a valid game state".replace("{{state}}", args[0]));
                }
            } else {
                Utils.broadcastMessage(Globals.gameState.toString());
            }
            return true;
        }
        return false;
    }

    public void setup() {
        int x = 0;
        for (File f : new File("./").listFiles()) {
            if (f.isDirectory())
                if (new File(f.getName() + "/endonauts_world.mark").exists()) {
                    getServer().unloadWorld(f.getName(), false);
                    WorldGenHandler.recursiveDelete(f);
                }
        }
        long id = (long) (Math.random() * Long.MAX_VALUE);
        WorldGenHandler.generateWorld(id);
        Globals.gameWorld = getServer().getWorld("" + id);
    }
}

class TEMP implements Listener // ONLY FOR CONSTRUCTION PURPOSES (TEMPORARY)
{
    @EventHandler
    public void event(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
            if (Main.TEMPDATA.containsKey(e.getPlayer())) {
                if (Main.TEMPDATA.get(e.getPlayer()).start() != null && !Main.TEMPDATA.get(e.getPlayer()).start()
                        .equals(e.getClickedBlock().getLocation())) {
                    try {
                        StructureIO
                                .readToFile(Main.TEMPDATA.get(e.getPlayer()).start(), e.getClickedBlock().getLocation(),
                                        Main.TEMPDATA.get(e.getPlayer()).s, new Material[]{Material.BARRIER});
                    } catch (IOException e1) {
                        System.out.println(e1.getMessage());
                    }
                    Main.TEMPDATA.remove(e.getPlayer());
                    return;
                }
                Main.TEMPDATA.get(e.getPlayer()).start(e.getClickedBlock().getLocation());
            }

    }
}

class Data {
    public final String s;
    private Location start;

    public Data(String n) {
        s = n;
        start = null;
    }

    public void start(Location l) {
        start = l.clone();
    }

    public Location start() {
        return start;
    }
}