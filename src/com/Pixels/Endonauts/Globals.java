package com.Pixels.Endonauts;

import com.Pixels.Endonauts.projectile.Projectile;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.CachedServerIcon;

import java.util.ArrayList;
import java.util.HashMap;

public class Globals {
//    public static final HashMap<Integer, Material> DECODEMATERIAL = new HashMap<>();
//    public static final HashMap<Material, Integer> ENCODEMATERIAL = new HashMap<>();
    public static final int[] charge = new int[]{32, 60, 132, 132, 251, 251, 251, 1562, 1562};
    public static final int[] maxAmount = new int[]{1, 64, 16, 9, 9, 9, 9, 16};
    public static int[] recharge = new int[]{1, 60, 3, 6, 12, 12, 12, 3, 10};
    public static final int[] nonFastRecharge = recharge;
    public static boolean isDebug = false;
    public static final Material[] tools = new Material[]{Material.GOLDEN_SWORD, Material.WOODEN_SHOVEL, Material.STONE_SHOVEL,
            Material.STONE_PICKAXE, Material.IRON_PICKAXE, Material.IRON_AXE, Material.IRON_SHOVEL, Material.DIAMOND_HOE,
            Material.DIAMOND_SHOVEL};
    public static final Material[] placeholders = new Material[]{Material.GOLDEN_SWORD, Material.RED_DYE, Material.GREEN_DYE,
            Material.BROWN_DYE, Material.BLUE_DYE, Material.PURPLE_DYE, Material.CYAN_DYE, Material.LIGHT_GRAY_DYE, Material.GRAY_DYE};
//    public static final String[] toolNames = new String[]{"Fireworks Beam", "Railgun", "Fireworks Beam MkII", "Mine",
//            "Missile", "Antimatter Beam", "Matter Beam", "Chaff", "Teleport"};
    public static World gameWorld = null;
    public static final ArrayList<Projectile> projectiles = new ArrayList<>();
    // IDLE (LOOP),
    // STARTING
    // (InventoryMenuHandler), SETUP
    // (), INGAME (),
    // DEATHMATCH (), ENDGAME ()

    public static int deathmatchTimer = -1;
    public static int startTimer = -1;
    public static final String chatName =
            ChatColor.DARK_GRAY + "[" + ChatColor.DARK_PURPLE + "Endo" + ChatColor.DARK_PURPLE + "n"
                    + ChatColor.DARK_PURPLE + "auts" + ChatColor.DARK_GRAY + "] " + ChatColor.AQUA;

    public static CachedServerIcon ICON = null;

    public static ArrayList<Player> players = new ArrayList<>();// Playahs
    public static GameState gameState = GameState.LOADING;// LOADING (WorldGenHandler),

    public static Main main = null;
}
