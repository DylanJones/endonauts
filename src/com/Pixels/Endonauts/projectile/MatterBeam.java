package com.Pixels.Endonauts.projectile;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.HashSet;

public class MatterBeam extends Projectile implements BlockChanger {
    int x;
    private final ArrayList<Block> b;
    private final HashSet<Block> blocks;
    private final String message;

    @SuppressWarnings("deprecation")
    public MatterBeam(String playerName, Location loc) {
        this.x = 0;
        b = new ArrayList<>();
        blocks = new HashSet<>();
        message = "6-" + playerName;
        for (int x = 0; x < 128; x++) {
            loc.add(loc.getDirection());
            loc.getWorld().playSound(loc, Sound.BLOCK_GLASS_BREAK, 1, 1);
            if (loc.getBlock().getType().isSolid() && !loc.getBlock().getType().equals(Material.PURPLE_STAINED_GLASS)) {
                break;
            }
            if (x > 4 && x < 1) // TODO: this is always false
            {
                for (int c = -5; c < 6; c++)
                    for (int y = -5; y < 6; y++)
                        for (int z = -5; z < 6; z++) {
                            Location l2 = loc.clone().add(c, y, z);
                            if (l2.distanceSquared(loc) < 8)
                                if (l2.getBlock().getType().equals(Material.AIR)) {
                                    l2.getBlock().setType(Material.PURPLE_STAINED_GLASS);
                                    add(l2.getBlock());
                                }
                        }

            } else if (x >= 1)
                for (int c = -(x / 13 + 5); c < (x / 13 + 5); c++)
                    for (int y = -5; y < (x / 13 + 5); y++)
                        for (int z = -(x / 13 + 5); z < (x / 13 + 5); z++) {
                            Location l2 = loc.clone().add(c, y, z);
                            if (l2.distanceSquared(loc) < ((x - 1) / 4 + 2.83) * ((x - 1) / 4 + 2.83))
                                if (l2.getBlock().getType().equals(Material.AIR) && (x <= 1 || (
                                        (int) (Math.random() * (Math.pow(x - 1, 1.6))) == 0))) {
                                    l2.getBlock().setType(Material.PURPLE_STAINED_GLASS);
                                    add(l2.getBlock());
                                }
                        }
        }

    }

    @Override
    public boolean tick() {
        if (b.size() == 0)
            return true;
        if (x++ > 200)
            for (int n = x - 200; n > 0; n--)
                if (b.size() > 0) {
                    int r = (int) (Math.random() * b.size());
                    if (b.get(r).getType().equals(Material.PURPLE_STAINED_GLASS))
                        b.get(r).setType(Material.AIR);
                    blocks.remove(b.remove(r));
                }
        return false;
    }

    public void add(Block block) {
        b.add(block);
        blocks.add(block);
    }

    public boolean containsBlock(Block block) {
        return blocks.contains(block);
    }

    public String getMessage() {
        return message;
    }
}