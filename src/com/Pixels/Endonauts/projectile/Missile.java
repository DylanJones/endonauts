package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;

public class Missile extends Projectile {
    @SuppressWarnings("FieldCanBeLocal")
    private final int SPEED = 2;
    public Entity target;
    private final String message;
    private final Location pos;
    private int ttl;

    public Missile(Player t, String m, Location loc) {
        target = t;
        this.pos = loc;
        message = "4-" + m;
        ttl = 0;
    }

    public boolean tick() {
        ttl++;
        double scalar = (ttl <= 10) ? 0.1 * ttl : 1;
        for (int n = 0; n < SPEED; n++) {
            if (ttl > 200 || pos.getBlock().getType().isSolid()) {
                explode(false);
                return true;
            }
            if(target != null)
            {
                Location targetLoc = target.getLocation().add(0, 0.5, 0);
                if (targetLoc.distanceSquared(pos) < 9) {
                    explode(true);
                    return true;
                }
                    targetLoc.add(target.getVelocity().multiply(targetLoc.distance(pos) / 16));
                    targetLoc.subtract(pos).multiply(-1);
                    // targetLoc = pos.clone().subtract(targetLoc);
                    float yaw = (float) (Math.atan(targetLoc.getZ() / targetLoc.getX()) * 180 / Math.PI + 90);
                    float pitch = (float) (Math.atan2(targetLoc.getY(), targetLoc.getX()) * 180 / Math.PI);
    
                    pos.add(pos.getDirection().multiply(1 - scalar));
                    pos.setYaw(yaw);
                    pos.setPitch(pitch);
                    pos.add(pos.getDirection().multiply(scalar));
            }
            else
                pos.add(pos.getDirection().multiply(1));
            
            pos.getWorld().spawnParticle(Particle.END_ROD, pos, 0);
            pos.getWorld().spawnParticle(Particle.SPELL_WITCH, pos, 0);
        }
        return false;
    }

    private void explode(boolean hitPlayer) {
        ArrayList<Firework> r = new ArrayList<>();
        if (hitPlayer) {
            Firework f = pos.getWorld().spawn(target.getLocation(), Firework.class);
            f.setCustomName(message);
            // m.getServer().getPlayer(target).damage(10, f);
            FireworkMeta fm = f.getFireworkMeta();
            fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(false).build());
            fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(false).build());
            fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(false).build());
            fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(false).build());
            f.setFireworkMeta(fm);
            // f.detonate();
            r.add(f);
            if (target instanceof Player) {
                Entity e = target.getWorld().spawnEntity(target.getLocation(), EntityType.ARMOR_STAND);
                e.setCustomName(message);
                ((Player) target).damage(Math.random() * 5 + 5, e);
                e.remove();
            }
        }
        for (int n = 0; n < 6; n++) {
            Firework f = pos.getWorld()
                    .spawn(pos.clone().add(Math.random() * 10 - 5, Math.random() * 10 - 5, Math.random() * 10 - 5),
                            Firework.class);
            f.setCustomName(message);
            FireworkMeta fm = f.getFireworkMeta();
            fm.addEffect(FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.PURPLE).trail(true).build());
            fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.ORANGE).trail(true).build());
            f.setFireworkMeta(fm);
            // f.detonate();
            r.add(f);
        }
        if(target != null)
        {
            class RunLater implements Runnable {
                @Override
                public void run() {
                    target.setInvulnerable(true);
                    for (Firework f : r) {
                        f.detonate();
                    }
                    Globals.main.getServer().getScheduler()
                            .runTaskLater(Globals.main, () -> target.setInvulnerable(false), 1);
                }
            }
            Globals.main.getServer().getScheduler().runTaskLater(Globals.main, new RunLater(), 1);
        }
    }
}