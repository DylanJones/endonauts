package com.Pixels.Endonauts.projectile;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class Chaff extends Projectile {
    int x;
    private final Location l;
    private final Vector v;

    public Chaff(Location l) {
        this.l = l.clone();
        this.v = new Vector(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5);
        v.multiply(Math.random() + 1.0);
        x = 0;
    }

    @Override
    public boolean tick() {
        if (x++ > 128)
            return true;
        if (l.getBlock().getType().isSolid())
            return true;
        l.add(v);
        l.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, l, 0);
        return false;
    }
}