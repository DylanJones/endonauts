package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworksBeam extends Projectile {
    final Firework f;

    public FireworksBeam(Location l, Location l2, Player p) {
        int x = 0;
        while (x < 200) {
            l.add(p.getLocation().getDirection());
            x++;
            l.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, l, 0);
            if (l.getBlock().getType().isSolid()) {
                x = 200;
                l.add(p.getLocation().getDirection().multiply(-1));
            }
            for (Player t : Globals.players) {
                if (t.getWorld().equals(l.getWorld()) && t != p && t.getLocation().distanceSquared(l) < 9
                        && t.getLocation().distanceSquared(l) > t.getLocation().distanceSquared(l2))
                    x = 200;
            }
            l2 = l.clone();
        }
        f = l.getWorld().spawn(l, Firework.class);
        f.setCustomName("0-" + p.getName());
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(true).build());
        f.setFireworkMeta(fm);
        // f.detonate();
    }

    @Override
    public boolean tick() {
        f.detonate();
        return true;
    }

}
