package com.Pixels.Endonauts.projectile;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

class Rocket extends Projectile {
    int x;
    private final Location l;
    private final Vector v;

    public Rocket(Location l, Vector v) {
        this.l = l.clone();
        this.v = v;
        x = 0;
    }

    @Override
    public boolean tick() {
        if (x++ > 128)
            return true;
        l.add(v);
        if (l.getBlock().getType().isSolid())
            return true;
        if (x < 8)
            return false;
        Firework f = l.getWorld().spawn(l, Firework.class);
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect(FireworkEffect.builder().with(Type.BALL).withColor(Color.PURPLE).trail(false).build());
        f.setFireworkMeta(fm);
        f.detonate();
        return false;
    }
}