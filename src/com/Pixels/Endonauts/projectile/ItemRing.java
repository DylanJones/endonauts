package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import com.Pixels.Endonauts.VectorUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class ItemRing extends Projectile {
    private final Location pos;
    private int time = 0;
    private final double radius;
    private final int refillGroup;

    public ItemRing(Location currentPos) {
        this.pos = currentPos;
        this.pos.setPitch(0);
        radius = 2;
        this.refillGroup = (int) (Math.random() * 3);
    }

    @Override
    public boolean tick() {
        if (++time > 1200)
            return true;
        if (time % 5 == 0) {
            if (refillGroup == 0)
                VectorUtils.drawRing(pos, Particle.DRAGON_BREATH, radius, 50);
            else if (refillGroup == 1)
                VectorUtils.drawRing(pos, Particle.FLAME, radius, 50);
            else if (refillGroup == 2)
                VectorUtils.drawRing(pos, Particle.VILLAGER_HAPPY, radius, 50);
        }
        return checkPlayers();
    }

    private boolean checkPlayers() {
        for (Player p : Globals.players) {
            Location playerLocation = p.getEyeLocation().clone();
            if (p.isGliding() && !p.isSneaking())
                playerLocation.add(0, -1.25, 0);
            if (playerLocation.distanceSquared(pos) < (radius + 1) * (radius + 1)) {
                Vector uFacing = pos.getDirection();
                Vector pFacing = p.getVelocity();
                if (Math.abs(uFacing.angle(pFacing)) < Math.toRadians(45)
                        || Math.abs(uFacing.clone().multiply(-1).angle(pFacing)) < Math.toRadians(45)) {
                    p.playSound(pos, Sound.ENTITY_ARROW_HIT_PLAYER, 1, (float) 0.5);
                    int[] slots;
                    switch (refillGroup) {
                        case 0:
                            slots = new int[]{0, 1, 2};
                            break;
                        case 1:
                            slots = new int[]{3, 4};
                            break;
                        case 2:
                            slots = new int[]{5, 6};
                            break;
                        default:
                            throw new RuntimeException("CORRUPT THINGS");
                    }
                    for (int slot : slots) {
                        ItemStack i = p.getInventory().getItem(slot);
                        i.setAmount(i.getAmount() + 3);
                        i.setType(Globals.tools[slot]);
                        // int slot = p.getInventory().getHeldItemSlot();
                        if (i.getAmount() > Globals.maxAmount[slot]) {
                            i.setAmount(Globals.maxAmount[slot]);
                        }
                        p.getInventory().setItem(slot, i);
                    }
                    return true;
                }
            }
        }
        return false;
    }
}