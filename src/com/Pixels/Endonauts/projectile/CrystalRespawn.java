package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

public class CrystalRespawn extends Projectile implements BlockChanger{
    private final Location l;
    private int x;

    public CrystalRespawn(Location l) {
        this.l = l;
        x = 600;
    }

    @Override
    public boolean tick() {
        if (x-- == 0) {
            Globals.gameWorld.spawnEntity(l, EntityType.ENDER_CRYSTAL);
            return true;
        }
        return false;

    }

}
