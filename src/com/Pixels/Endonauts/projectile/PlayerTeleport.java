package com.Pixels.Endonauts.projectile;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class PlayerTeleport extends Projectile {
    public PlayerTeleport(Location l, Location l2, Player p) {
        int x = 0;
        Vector v = p.getVelocity();
        while (x < 64) {
            l.add(p.getLocation().getDirection());
            x++;
            l.getWorld().spawnParticle(Particle.SPELL_WITCH, l, 0);
            l.getWorld().spawnParticle(Particle.DRAGON_BREATH, l, 0);
            if (!l.getWorld().getBlockAt(l).getType().isSolid() && !l.clone().add(0, 1, 0).getBlock().getType()
                    .isSolid())
                l2 = l.clone();
        }
        p.teleport(l2);
        if (l2.distanceSquared(l) == 0) {// This if statement seems to
            // always return true. Not sure
            // why :(
            p.setVelocity(v);
        } else {
            p.setVelocity(new Vector(0, 0, 0));
        }
    }

    @Override
    public boolean tick() {
        return true;
    }

}
