package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworksBeamII extends Projectile {
    private final Firework f;

    public FireworksBeamII(Location l, Location l2, Player p) {
        int x = 0;
        while (x < 200) {
            l.add(p.getLocation().getDirection());
            x++;
            l.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, l, 0);
            if (l.getBlock().getType().isSolid()) {
                x = 200;
                l.add(p.getLocation().getDirection().multiply(-1));
            }
            for (Player t : Globals.players) {
                if (t.getWorld().equals(l.getWorld()) && t != p && t.getLocation().distanceSquared(l) < 25
                        && t.getLocation().distanceSquared(l) > t.getLocation().distanceSquared(l2))
                    x = 200;
            }
            l2 = l.clone();
        }
        f = l.getWorld().spawn(l, Firework.class);
        f.setCustomName("2-" + p.getName());// DEATH
        // MESSAGE
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect(FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.PURPLE).trail(true).build());
        fm.addEffect(FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.PURPLE).trail(true).build());
        f.setFireworkMeta(fm);
        // f.detonate();
    }

    @Override
    public boolean tick() {
        f.detonate();
        return true;
    }

}
