package com.Pixels.Endonauts.projectile;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class AntimatterBeam extends Projectile implements BlockChanger {
    private final HashMap<Location, BlockState> blocks;
    private final ArrayList<Location> locs;
    int x;

    public AntimatterBeam(Location l) {
        x = 0;
        blocks = new HashMap<>();
        locs = new ArrayList<>();
        for (int i = 0; i < 64; i++) {
            l.add(l.getDirection());
            l.getWorld().playSound(l, Sound.BLOCK_CHORUS_FLOWER_DEATH, 1, 1);
            for (int c = -5; c < 6; c++)
                for (int y = -5; y < 6; y++)
                    for (int z = -5; z < 6; z++) {
                        Location l2 = l.clone().add(c, y, z);
                        if (l2.distanceSquared(l) < 8)
                            if (!l2.getBlock().getType().equals(Material.AIR) && !l2.getBlock().getType()
                                    .equals(Material.END_GATEWAY) && l2.getBlockY() >= 0) {
                                if (!l2.getBlock().getType().equals(Material.PURPLE_STAINED_GLASS)) {
                                    add(l2.getBlock());
                                }
                                l2.getBlock().setType(Material.AIR, false);
                            }
                    }
        }
    }

    @Override
    public boolean tick() {
        if (blocks.size() == 0)
            return true;
        if (x == 200) {
            Collections.shuffle(locs);
        }
        if (x++ > 200) {
            for (int n = x - 200; n > 0; n--)
                if (blocks.size() > 0) {
                    Location l = locs.remove(locs.size() - 1);
                    BlockState h = blocks.remove(l);
                    h.update(true, false);
                }
        }
        return false;
    }

    public void add(Block block) {
        BlockState state = block.getState();
        blocks.put(block.getLocation(), state);
        locs.add(state.getLocation());
    }
}