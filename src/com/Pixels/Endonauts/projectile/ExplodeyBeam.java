package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ExplodeyBeam extends Projectile implements BlockChanger {

    private final HashMap<Location, BlockState> blocks;
    private final ArrayList<Location> locs;
    private int x;

    public ExplodeyBeam(Location l, Player src) {
        x = 0;
        blocks = new HashMap<>();
        locs = new ArrayList<>();
        for (int i = 0; i < 64; i++) {
            l.add(l.getDirection());
            l.getWorld().spawnParticle(Particle.REDSTONE, l, 0, 0, 0, 0, new Particle.DustOptions(Color.RED, 1));
            l.getWorld().playSound(l, Sound.BLOCK_CHORUS_FLOWER_DEATH, 1, 1);
            for (Player p : Globals.players) {
                if ((p.getWorld().equals(l.getWorld()) && l.distanceSquared(p.getLocation()) < 36 && p != src)
                        || l.getBlock().getType().isSolid()) {
                    explode(l, src);
                    return;
                }
            }
        }
        explode(l, src);
    }

    @Override
    public boolean tick() {
        if (blocks.size() == 0)
            return true;
        if (x == 200) {
            Collections.shuffle(locs);
        }
        if (x++ > 200)
            for (int n = x - 200; n > 0; n--)
                if (blocks.size() > 0) {
                    Location l = locs.remove(locs.size() - 1);
                    BlockState h = blocks.remove(l);
                    h.update(true, false);
                }
        return false;
    }

    public void add(Block block) {
        BlockState state = block.getState();
        blocks.put(block.getLocation(), state);
        locs.add(state.getLocation());
    }

    private void explode(Location l, Player src) {
        int radius = 11;
        for (int c = -radius; c <= radius; c++)
            for (int y = -radius; y <= radius; y++)
                for (int z = -radius; z <= radius; z++) {
                    Location l2 = l.clone().add(c, y, z);
                    if (l2.distanceSquared(l) < radius * radius)
                        if (!l2.getBlock().getType().equals(Material.AIR)
                                && !l2.getBlock().getType().equals(Material.END_GATEWAY)) {
                            if (!l2.getBlock().getType().equals(Material.PURPLE_STAINED_GLASS))
                                add(l2.getBlock());
                            l2.getBlock().setType(Material.AIR, false);
                            if (Math.random() < 0.004) {
                                l2.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, l2, 1);
                            }
                        }
                }
        l.getWorld().playSound(l, Sound.ENTITY_GENERIC_EXPLODE, 5, 0.8f);
        for (Player p : Globals.gameWorld.getPlayers()) {
            if (p.getLocation().distanceSquared(l) < 36) {
                ArmorStand e = (ArmorStand) p.getWorld().spawnEntity(new Location(p.getWorld(), 0, 0, 0),
                        EntityType.ARMOR_STAND);
                e.setCustomName("5-" + src.getName());
                e.setVisible(false);
                p.damage(Math.random() * 5 + 5, e);
                e.remove();
            }
        }
    }

}
