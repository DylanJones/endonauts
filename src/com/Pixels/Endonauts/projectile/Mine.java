package com.Pixels.Endonauts.projectile;

import com.Pixels.Endonauts.Globals;
import com.Pixels.Endonauts.VectorUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Mine extends Projectile implements BlockChanger {
    private final HashMap<Location, BlockState> blocks;
    private final ArrayList<Location> locs;
    int ttl;
    final Player placer;
    final Location loc;
    final double radius;
    final double blastRadiusSquared;
    int x;

    public Mine(Player placer, Location placeLoc) {
        ttl = 2400; // 2 minutes
        this.placer = placer;
        loc = placeLoc;
        this.radius = 0.5;
        blastRadiusSquared = 100;
        x = 0;
        blocks = new HashMap<>();
        locs = new ArrayList<>();
    }

    @Override
    public boolean tick() {
        if (x == 0) {
            if (--ttl == 0) {
                return true;
            }
            Location tmp = loc.clone();
            tmp.setYaw(ttl);
            tmp.setPitch(90);
            VectorUtils.drawRing(tmp, Particle.TOWN_AURA, radius, 10);
            tmp.setPitch(0);
            tmp.setYaw(90 - ttl);
            VectorUtils.drawRing(tmp, Particle.TOWN_AURA, radius, 10);
            tmp.setYaw(90);
            tmp.setPitch(90 - ttl);
            VectorUtils.drawRing(tmp, Particle.TOWN_AURA, radius, 10);
            // placer.getWorld().spawnParticle(Particle.HEART, tmp, 2);
            // Collide w/ players
            for (Player p : Globals.players) {
                if (p.getLocation().distanceSquared(loc) < blastRadiusSquared && !p.equals(placer) && !p.getGameMode().equals(GameMode.SPECTATOR)) {
                    p.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 3, 1);
                    p.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, loc, 8, 3, 3, 3);
                    Entity e = p.getWorld().spawnEntity(p.getLocation(), EntityType.ARMOR_STAND);
                    e.setCustomName("4-" + placer.getName());
                    p.damage(Math.random() * 4 + 8, e);
                    e.remove();
                    // EXPLODE
                    x++;
                    for (int c = -10; c <= 10; c++)
                        for (int y = -10; y <= 10; y++)
                            for (int z = -10; z <= 10; z++) {
                                Location l2 = loc.clone().add(c, y, z);
                                if (l2.distanceSquared(loc) < 10 * 10)
                                    // Don't blow up end gateways - they have extra data we want to keep
                                    if (!l2.getBlock().getType().equals(Material.AIR) && !l2.getBlock().getType()
                                            .equals(Material.END_GATEWAY)) {
                                        if (!l2.getBlock().getType().equals(Material.PURPLE_STAINED_GLASS)) {
                                            // Purple stained glass shouldn't come back
                                            add(l2.getBlock());
                                        }
                                        l2.getBlock().setType(Material.AIR, false);
                                        if (Math.random() < 0.004) {
                                            l2.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, l2, 1);
                                        }
                                    }
                            }
                }
            }
            return false;
        } else {
            if (blocks.size() == 0)
                return true;
            if (x == 200) {
                Collections.shuffle(locs);
            }
            if (x++ > 200)
                for (int n = x - 200; n > 0; n--)
                    if (blocks.size() > 0) {
                        Location l = locs.remove(locs.size() - 1);
                        BlockState h = blocks.remove(l);
                        h.update(true, false);
                    }
            return false;
        }
    }

    public void add(Block block) {
        BlockState state = block.getState();
        blocks.put(block.getLocation(), state);
        locs.add(state.getLocation());
    }
}
