package com.Pixels.Endonauts.projectile;

public abstract class Projectile {
    public abstract boolean tick();
}