package com.Pixels.Endonauts;

public class DeathMessageHandler {
    private static final String[][] messages = new String[][]{
            // Firework - 0
            new String[]{"was Firework'd by [NAME]", "couldn't take the basics from [NAME]",
                    "went down at the hands of [NAME]", "didn't read [NAME]'s EULA"},
            // Railgun - 1
            new String[]{"was shot by [NAME]", "was derailed by [NAME]",
                    "experienced blunt force trauma at the hands of [NAME]"},
            // Firework MkII - 2
            new String[]{"was killed in a blaze of sparkles by [NAME]", "was BIG FIREWORK'D by [NAME]",
                    "was MkII'd by [NAME]", "met [NAME]'s bigger friend"},
            // Mine - 3
            new String[]{"was blown up by [NAME]", "was Note 7'd by [NAME]",
                    "stepped in the wrong side of [NAME]'s garden"},
            // Missile - 4
            new String[]{"was traced down by [NAME]", "couldn't outrun a rocket from [NAME]",
                    "was caught in [NAME]'s sights", "was seen by [NAME] from a mile away"},
            // Antimatter beam - 5
            new String[]{"was annihilated by [NAME]", "met their anti-self at the hands of [NAME]",
                    "met modern physics with the help of [NAME]"},
            // Matter beam - 6
            new String[]{"ran into a wall built by [NAME]", "was flying too fast to see [NAME]",
                    "pancaked on [NAME]'s matter beam", "performed a lithobraking maneuver at [NAME]'s request"},
            // ??? - 7
            new String[]{},
            // Ender crystal - 8
            new String[]{"had their life force removed by [NAME]",
                    "was ripped out of the mortal plane by [NAME]"}};

    public static String handleMessage(String input) {
        int id;
        String name;
        try {
            name = input.split("-")[1];
            id = Integer.parseInt(input.split("-")[0]);
            String message = messages[id][(int) (Math.random() * messages[id].length)];
            return " " + message.replace("[NAME]", name);
        } catch (Exception e) {
            return " death message error " + e.toString();
        }

    }
}
