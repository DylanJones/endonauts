package com.Pixels.Endonauts;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class PacketInterceptor extends PacketAdapter {
    public PacketInterceptor(Plugin main, PacketType entityMetadata) {
        super(main, entityMetadata);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void onPacketSending(PacketEvent event) {
        if (event.getPacketType().equals(PacketType.Play.Server.ENTITY_METADATA)) {
            try {
                Player dest = event.getPlayer();
                PacketContainer packet = event.getPacket();
                Object handle = reflectGet(packet, "handle");
                ArrayList l = (ArrayList) reflectGet(handle, "b");
                Integer entId = (Integer) reflectGet(handle, "a");
                List<Entity> entities = dest.getWorld().getEntities();
                Player glower = null;
                for (Entity e : entities) {
                    if (e.getEntityId() == entId) {
                        glower = (Player) e;
                    }
                }
                boolean glow = true;
                if (glower == null)
                    glow = false;
                assert glower != null;
                if (!(dest.hasMetadata("lookingAt") && dest.getMetadata("lookingAt").get(0).asString()
                        .equals(glower.getName())))
                    glow = false;
                byte data = (Byte) reflectGet(l.get(0), "b");
                if (glow) {
                    data |= 0x40;
                } else {
                    data &= ~0x40;
                }
                Byte stat2 = data;
                reflectSet(l.get(0), "b", stat2);
            } catch (Exception ignored) {
            }
        }

    }

    private void reflectSet(Object thing, String field, Object value) {
        try {
            Field f = thing.getClass().getDeclaredField(field);
            f.setAccessible(true);
            f.set(thing, value);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ignored) {
        }

    }

    private Object reflectGet(Object thing, String field) {
        try {
            Field f = thing.getClass().getDeclaredField(field);
            f.setAccessible(true);
            return f.get(thing);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}