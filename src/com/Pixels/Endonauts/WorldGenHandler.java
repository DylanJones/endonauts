package com.Pixels.Endonauts;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Stack;

//Class in charge of world generation.  WIP
public class WorldGenHandler {
    public static void generateWorld(long seed) {
        Globals.projectiles.clear();

        WorldCreator wc = new WorldCreator("" + seed);
        wc.seed(seed);
        wc.environment(Environment.THE_END);
        World next = wc.createWorld();
        next.setGameRuleValue("naturalRegeneration", "false");
        next.setGameRuleValue("keepInventory", "true");
        next.setGameRuleValue("doMobSpawning", "false");

        WorldStructureGenerator.generate(seed, next);

        for (Player p : Globals.main.getServer().getOnlinePlayers()) {
            p.teleport(new Location(next, 0, 150, 0));
            p.setBedSpawnLocation(new Location(next, 0, 150, 0), true);
        }

        if (Globals.gameWorld != null) {
            Globals.main.getServer().unloadWorld(Globals.gameWorld, true);
            recursiveDelete(new File("" + Globals.gameWorld.getName()));
        }

        Globals.gameWorld = next;
        File f = new File(Globals.gameWorld.getName() + File.separator + "endonauts_world.mark");// '/' used to be File.pathSeperator?
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
        } catch (IOException ignored) {
            System.out.println("[WARNING] Failed to generate endonauts world mark");
        }
    }

    public static void recursiveDelete(File start) {
        Stack<File> s = new Stack<>();
        Stack<File> remove = new Stack<>();// Doesn't need to be stack, but why not
        s.push(start);
        while (!s.isEmpty()) {
            File f = s.pop();
            if (f.isDirectory()) {

                for (File subF : f.listFiles())
                    s.push(subF);
            }
            remove.push(f);
        }
        while (!remove.isEmpty()) {
            remove.pop().delete();
        }
    }
}
