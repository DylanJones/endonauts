package com.Pixels.Endonauts;

import com.Pixels.Endonauts.generator.Structure;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Arrays;

public class GameStateChangeHandler {
    private static int dmTimer = 0;

    public static void starting() {
        Globals.startTimer--;
        if (Globals.startTimer == 0) {
            Utils.broadcastMessage("Starting game...");
            Main.changeState(GameState.SETUP);
            Globals.startTimer = 100;
            platforms(true);
            int angle = 0;
            Location l = new Location(Globals.gameWorld, 0, 150, 0);
            for (Player p : Globals.players) {
                p.setGameMode(GameMode.SURVIVAL);
                int x = (int) (Math.cos(Math.toRadians(angle)) * 42);
                int z = (int) (Math.sin(Math.toRadians(angle)) * 42);
                l.setX(x + 0.5);
                l.setZ(z + 0.5);
                l.setPitch(0);
                l.setYaw(angle + 90);
                p.teleport(l);
                angle += 36;
                if (angle >= 360)
                    angle = 0;
            }
        }
    }

    public static void setup() {
        for (Player p : Globals.gameWorld.getPlayers()) {
            p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 1000000, 9), true);
            p.setHealth(60);
        }
        if (Globals.startTimer % 20 == 0 && Globals.startTimer > 0) {
            Utils.broadcastMessage("Game starting in " + Globals.startTimer / 20 + "...");
            for (Player p : Globals.gameWorld.getPlayers())
                p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
        }
        if (Globals.startTimer == 0) {
            platforms(false);
            Utils.broadcastMessage("Go!");
            setupHotbars();
            Main.changeState(GameState.INGAME);
            Globals.deathmatchTimer = 24; // 2400 = 2 minute, 6000 = 5 minute
            // maybe?
            for (Player p : Globals.gameWorld.getPlayers())
                p.playSound(p.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, 1, 1);
        }
        Globals.startTimer--;
    }

    public static void ingame() {
        if (Globals.deathmatchTimer > 0) {
            Globals.deathmatchTimer--;
            for (Player p : Globals.gameWorld.getPlayers())
                InventoryMenuHandler.deathmatchButtonState(p, false);
        }
        if (Globals.deathmatchTimer == 0) {
            for (Player p : Globals.gameWorld.getPlayers())
                InventoryMenuHandler.updatePlayerMenu(p, false);
            Globals.deathmatchTimer--;
        }
    }

    public static void deathmatch() {
        Globals.deathmatchTimer++;
        if ((Globals.startTimer % 20 == 0 && Globals.startTimer > 0)) {
            if (Globals.startTimer % 100 == 0 || Globals.startTimer < 100)
                Utils.broadcastMessage("Deathmatch in " + Globals.startTimer / 20 + "...");
            for (Player p : Globals.gameWorld.getPlayers())
                p.playSound(p.getLocation(), Sound.BLOCK_DISPENSER_FAIL, 1, 1);
        }
        if (Globals.startTimer == 0) {
            Utils.broadcastMessage("Go!");
            for (Player p : Globals.gameWorld.getPlayers())
                p.playSound(p.getLocation(), Sound.ENTITY_WITHER_DEATH, 1, 1);

            int angle = 0;
            Location l = new Location(Globals.gameWorld, 0, 150, 0);
            for (Player p : Globals.players) {
                p.setVelocity(new Vector(0, 0, 0));
                int x = (int) (Math.cos(Math.toRadians(angle)) * 35);
                int z = (int) (Math.sin(Math.toRadians(angle)) * 35);
                l.setX(x + 0.5);
                l.setZ(z + 0.5);
                p.teleport(l);
                angle += 36;
                if (angle >= 360)
                    angle = 0;
            }
            Globals.recharge = Arrays.copyOf(Globals.recharge, Globals.recharge.length);
            for (int x = 0; x < Globals.recharge.length; x++)
                Globals.recharge[x] = Globals.recharge[x] * 10;
            dmHotbars();
        }
        if (Globals.startTimer < 0) {
            dmHotbarsTick();
            limitSpace();
            dmbarrier();
        }
        if (Globals.startTimer >= 0)
            Globals.startTimer--;

    }

    public static void endgame() {
        Globals.startTimer--;
        if (Globals.startTimer == 0) {
            for (Player p : Globals.main.getServer().getOnlinePlayers()) {
                p.setGameMode(GameMode.SPECTATOR);
                p.getInventory().clear();
            }
            Globals.recharge = Globals.nonFastRecharge;
            Structure.removeGateways();
            Utils.broadcastMessage("Select the 'Play' button to start another game");
            Main.changeState(GameState.IDLE);
        }
        resetEnderCrystals();
    }

    private static void platforms(boolean build) {
        Location l = new Location(Globals.gameWorld, 0, 150, 0);
        for (int angle = 0; angle < 360; angle += 36) {
            int x = (int) (Math.cos(Math.toRadians(angle)) * 42);
            int z = (int) (Math.sin(Math.toRadians(angle)) * 42);
            l.setX(x);
            l.setZ(z);
            if (build)
                StructureIO.buildStructure(l.clone().add(-1, -2, -1), "startplatform");
            else
                Structure.fillArea(l.clone().add(-1, -2, -1), l.clone().add(1, 3, 1), Material.AIR);
        }
    }

    private static void setupHotbars() {
        for (Player p : Globals.players) {
            Inventory i = p.getInventory();
            ItemStack is = new ItemStack(Material.GOLDEN_SWORD);

            /* * Fireworks Beam **/
            i.setItem(0, is);

            /* * Railgun **/
            is.setType(Material.WOODEN_SHOVEL);
            is.setAmount(64);
            i.setItem(1, is);

            /* * Fireworks Beam II **/
            is.setType(Material.STONE_SHOVEL);
            is.setAmount(4);
            i.setItem(2, is);

            /* * Mine **/
            is.setType(Material.BROWN_DYE);
            is.setAmount(1);
            is.setDurability((short) 3);
            i.setItem(3, is);

            /* * Missile **/
            is.setType(Material.BLUE_DYE);
            is.setAmount(1);
            is.setDurability((short) 4);
            i.setItem(4, is);

            /* * Antimatter Beam **/
            is.setType(Material.PURPLE_DYE);
            is.setAmount(1);
            is.setDurability((short) 5);
            i.setItem(5, is);

            /* * Matter Beam **/
            is.setType(Material.CYAN_DYE);
            is.setAmount(1);
            is.setDurability((short) 6);
            i.setItem(6, is);

            /* * Chaff **/
            is.setType(Material.DIAMOND_HOE);
            is.setAmount(4);
            i.setItem(7, is);

            /* * Teleport **/
            is.setType(Material.DIAMOND_SHOVEL);
            is.setAmount(8);
            i.setItem(8, is);
        }
    }

    private static void dmHotbars() {
        for (Player p : Globals.players) {
            for (int x = 0; x < 7; x++) {
                ItemStack i = new ItemStack(Globals.tools[x]);
                i.setAmount(Globals.maxAmount[x]);
                i.setType(Globals.tools[x]);
                p.getInventory().setItem(x, i);
            }
            p.getInventory().clear(7);
            p.getInventory().clear(8);
        }
    }

    private static void dmHotbarsTick() {
        for (Player p : Globals.players) {
            for (int x = 1; x < 7; x++) {
                if (p.getInventory().getItem(x).getType().equals(Material.BLACK_DYE))
                    p.getInventory().setItem(x, new ItemStack(Globals.tools[x]));
                else {
                    ItemStack item = p.getInventory().getItem(x);
                    item.setAmount(1);
                    p.getInventory().setItem(x, item);
                }
            }
        }
    }

    private static void limitSpace() {
        int radius = 96;
        for (Player p : Globals.players) {
            Location l = new Location(Globals.gameWorld, 0, p.getLocation().getY(), 0);
            if (p.getLocation().distanceSquared(l) > radius * radius) {
                l = p.getLocation().clone();
                l.setX(l.getX() * -0.95);
                l.setZ(l.getZ() * -0.95);
                int ttl = 0;
                while (l.getBlock().getType().isSolid() && ttl++ < 100) {
                    l.setX(l.getX() * 0.95);
                    l.setZ(l.getZ() * 0.95);
                }
                p.teleport(l);
                p.setVelocity(p.getVelocity());
                p.playSound(p.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1, 1);
            }
        }
    }

    private static void dmbarrier()// TODO This would be way easier by just
    // setting gateway age, not sure how to do
    // that though.
    {
        dmTimer++;
        if (dmTimer == 500)
            dmTimer = 0;
        if (dmTimer % 100 == 0) {
            Location l = new Location(Globals.gameWorld, 0, 75, 0);
            for (int angle = 0; angle < 360; angle += 18) {
                int x = (int) (Math.cos(Math.toRadians(angle)) * 96);
                int z = (int) (Math.sin(Math.toRadians(angle)) * 96);
                l.setX(x);
                l.setZ(z);
                l.getBlock().setType(Material.AIR);
                l.getBlock().setType(Material.END_GATEWAY);
            }
        }
    }
    
    private static void resetEnderCrystals()
    {
        for (EnderCrystal e : Globals.gameWorld.getEntitiesByClass(EnderCrystal.class))
            e.setBeamTarget(null);
    }
}
