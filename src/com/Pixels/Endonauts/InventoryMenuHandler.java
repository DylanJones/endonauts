package com.Pixels.Endonauts;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryMenuHandler implements Listener {
    public static Player build = null;
    public static boolean[] buttons = new boolean[]{false, false, false, false, false, false};

    @EventHandler
    public static void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (p.getGameMode().equals(GameMode.CREATIVE))
            return;
        if (!e.getInventory().getType().equals(InventoryType.CRAFTING))
            return;
        if (p.getGameMode().equals(GameMode.SURVIVAL)) {
            e.setCancelled(true);
            return;
        }
        p.playSound(p.getLocation(), Sound.BLOCK_STONE_PRESSURE_PLATE_CLICK_ON, (float) 0.75, 1);
        /* ********** SPECTATE ********/
        if ((e.getSlot() == 9 || e.getSlot() == 10 || e.getSlot() == 11) && buttons[0]) {
            if (Globals.players.contains(p)) {
                Globals.players.remove(p);
                p.sendMessage(Globals.chatName + "You are now a spectator");
            } else {
                Globals.players.add(p);
                p.sendMessage(Globals.chatName + "You are no longer a spectator");
            }
            updatePlayerMenu(p, false);
        }
        /* ********** BUILD MAP ********/
        if ((e.getSlot() == 15 || e.getSlot() == 16 || e.getSlot() == 17) && buttons[1]) {
            if (build != null) {
                if (build != p || Globals.gameWorld.getPlayers().size() == 1) {
                    build = null;// Other null set in play button stuff.
                    Main.changeState(GameState.LOADING);
                    Utils.broadcastMessage("Building new map.  This could take a few seconds.");
                    WorldGenHandler.generateWorld((long) (Math.random() * Long.MAX_VALUE * 2 - Long.MAX_VALUE));
                }
            } else {
                build = p;
                buildButtonState(p, false);
                Utils.broadcastMessage(
                        p.getName() + " has requested a new map. Second the request to begin map generation");
            }
        }
        /* ********** DEATHMATCH ********/
        if ((e.getSlot() == 18 || e.getSlot() == 19 || e.getSlot() == 20) && buttons[2] && Globals.deathmatchTimer == 0) {
            Globals.startTimer = 600;
            Main.changeState(GameState.DEATHMATCH);
            Utils.broadcastMessage(p.getName() + " has started the deathmatch!  Be ready.");
        }
        /* ********** PLAY **************/
        if ((e.getSlot() == 30 || e.getSlot() == 31 || e.getSlot() == 32) && buttons[3]) {
            Main.changeState(GameState.STARTING);
            build = null;
            Globals.startTimer = 200;
            Utils.broadcastMessage("The game will start in 10 seconds.  Press 'Cancel' to abort the start.");
            for (Player soundTo : Globals.gameWorld.getPlayers())
                if (soundTo != p)
                    soundTo.playSound(soundTo.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
        }
        /* ********** CANCEL ************/
        if ((e.getSlot() == 33) && buttons[4]) {
            Main.changeState(GameState.IDLE);
            Utils.broadcastMessage("Game start aborted");
            for (Player soundTo : Globals.gameWorld.getPlayers())
                if (soundTo != p)
                    soundTo.playSound(soundTo.getLocation(), Sound.BLOCK_GLASS_BREAK, 1, 1);
        }
        /* ********** INFO **************/
        if ((e.getSlot() == 26) && buttons[5]) {
            Inventory i = Bukkit.createInventory(p, InventoryType.SHULKER_BOX, "");

            p.openInventory(i);
            p.getInventory().clear();
            //
        }
        e.setCancelled(true);
    }

    public static void clearButtons(Player p) {
        spectateButtonState(p, false);
        buildButtonState(p, false);
        deathmatchButtonState(p, false);
        playButtonState(p, false);
        cancelButtonState(p, false);
        infoButtonState(p, false);
    }

    public static void updatePlayerMenu(Player p, boolean force) {
        if (p.getOpenInventory().getType().equals(InventoryType.SHULKER_BOX) && !force)
            return;
        switch (Globals.gameState) {
            case LOADING:
                buttons = new boolean[]{false, false, false, false, false, false};
                break;
            case IDLE:
                buttons = new boolean[]{true, true, false, true, false, true};
                break;
            case STARTING:
                buttons = new boolean[]{true, false, false, false, true, true};
                break;
            case SETUP:
                buttons = new boolean[]{false, false, false, false, false, false};
                break;
            case INGAME:
                buttons = new boolean[]{false, false, true, false, false, true};
                break;
            case DEATHMATCH:
                buttons = new boolean[]{false, false, false, false, false, true};
                break;
            case ENDGAME:
                buttons = new boolean[]{false, false, false, false, false, false};
                break;
        }
        spectateButtonState(p, buttons[0]);
        buildButtonState(p, buttons[1]);
        deathmatchButtonState(p, buttons[2]);
        playButtonState(p, buttons[3]);
        cancelButtonState(p, buttons[4]);
        infoButtonState(p, buttons[5]);
    }

    public static void playButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(30);
            i.clear(31);
            i.clear(32);
        } else {
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) 0);
            ItemMeta im = is.getItemMeta();
            is.setItemMeta(im);
            p.getInventory().setItem(30, is);
            is.setDurability((short) 1);
            p.getInventory().setItem(31, is);
            is.setDurability((short) 2);
            p.getInventory().setItem(32, is);
        }
        p.updateInventory();
    }

    public static void cancelButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(33);
        } else {
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) 3);
            p.getInventory().setItem(33, is);
        }
        p.updateInventory();
    }

    public static void buildButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(15);
            i.clear(16);
            i.clear(17);
        } else {
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) 4);
            p.getInventory().setItem(15, is);
            is.setDurability((short) 5);
            p.getInventory().setItem(16, is);
            is.setDurability((short) 6);
            p.getInventory().setItem(17, is);
        }
        p.updateInventory();
    }

    public static void spectateButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(9);
            i.clear(10);
            i.clear(11);
        } else {
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) 7);
            ItemMeta im = is.getItemMeta();
            is.setItemMeta(im);
            p.getInventory().setItem(9, is);
            is.setDurability((short) 8);
            p.getInventory().setItem(10, is);
            if(Globals.players.contains(p))
                is.setDurability((short) 9);
            else
                is.setDurability((short) 14);
            p.getInventory().setItem(11, is);
        }
        p.updateInventory();
    }

    public static void deathmatchButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(18);
            i.clear(19);
            i.clear(20);
        } else {
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) 10);
            ItemMeta im = is.getItemMeta();
            is.setItemMeta(im);
            p.getInventory().setItem(18, is);
            is.setDurability((short) 11);
            p.getInventory().setItem(19, is);
            is.setDurability((short) 12);
            p.getInventory().setItem(20, is);
        }
        p.updateInventory();
    }

    public static void infoButtonState(Player p, boolean usable) {
        Inventory i = p.getInventory();
        if (!usable) {
            i.clear(26);
        } else {
            ItemStack is = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
            p.getInventory().setItem(26, is);
        }
        p.updateInventory();
    }

    // UPDATE INVENTORY
    @EventHandler
    public void closeInventory(InventoryCloseEvent e) {
        if (e.getPlayer().getOpenInventory().getType().equals(InventoryType.SHULKER_BOX)) {
            e.getPlayer().getInventory().clear();
            updatePlayerMenu((Player) e.getPlayer(), true);
        }
    }

    @EventHandler
    public void playerDropItem(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void playerOpenInventory(InventoryOpenEvent e) {
        if (e.getPlayer().getGameMode().equals(GameMode.SURVIVAL))
            e.setCancelled(true);
    }

    @EventHandler
    public void playerSwapoffhand(PlayerSwapHandItemsEvent e) {
        e.setCancelled(true);
    }
}
