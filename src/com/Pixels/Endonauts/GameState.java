package com.Pixels.Endonauts;

public enum GameState {
    LOADING, IDLE, STARTING, SETUP, INGAME, DEATHMATCH, ENDGAME
}
