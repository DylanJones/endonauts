#!/bin/bash

rsync -varu --progress "$(dirname $0)"/ "$1":/tmp/remote_server/

ssh "$1" cd /tmp/remote_server/mcserver \; java -Xmx2G -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar spigot-1.14.4.jar
